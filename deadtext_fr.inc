healher:
	.db "La soigner?",0
choicetext1:
	.db "Akara est par terre,",0
choicetext2:
	.db "mourrante. Si tu utilise"
choicetext3:
	.db "toute votre vie, elle vivra,",0
choicetext4:
	.db "mais tu mourras.",0
badend1:
	.db "Un noble geste...",0
badend2:
	.db "Tu te jetes en enfer",0
badend3:
	.db "sachant que tu la verra",0
badend4:
	.db "..un jour.",0
cruelplace:
	.db "dans cette cruelle place votre",0
cruelplace2:
	.db "voix au dessus du tourbillon",0
endgame1:
	.db "Meme si",0
endgame2:
	.db "Akara était",0
endgame3:
	.db "ta bienne aimee,",0
endgame4:
	.db "fidele et",0
endgame5:
	.db "attentive,",0
endgame6:
	.db "des fois, la",0
endgame7:
	.db "vie est juste",0
endgame8:
	.db "plus importante.",0
lasttext1:
	.db "Tu vois Akara, capturee",0
lasttext2:
	.db "par le Sorcier Demon.",0
lasttext3:
	.db "Akara!!!",0
lasttext4:
	.db "Kurai, aide-moi!!!",0
lasttext5:
	.db "Le Sorcier dit:",0
lasttext6:
	.db "Elle m'appartient,",0
lasttext7:
	.db "dans la vie...",0
lasttext8:
	.db "...ou la mort!",0
lasttext9:
	.db "Le Sorcier jette Akara",0
lasttext10:
	.db "a terre et attaque!",0
Serpent1:
	.db "Un serpent apparait!",0
faitharmor:
	.db "l'armure de la Foi!",0
Serpent2:
	.db "Vainc-moi pour avoir",0
last:
	.db "L'ultime bataille?",0
acave:
	.db "Une caverne cachée!",0
saysnothing:
	.db "L'homme ne dit rien.",0
aswitch:
	.db "une switche!",0
OldManText1:
	.db "Ha, un sauveur, mais,",0
OldManText2:
	.db "si tu veux l'epee de foi",0
OldManText3:
	.db "c'est dans la caverne du nord. Voici",0
thekey:
	.db "la cle!",0
corpse:
	.db "Un corps apparait!",0
mykey:
	.db "ma cle..ma cle..",0
Locked:
	.db "C'est barre",0
Awakened:
	.db "Reveille",0
KuraiText1:
	.db "Euh? La porte",0
KuraiText2:
	.db "de la prison",0
KuraiText3:
	.db "est ouverte..",0
KuraiText4:
	.db "Tous les",0
KuraiText5:
	.db "gardes ont",0
KuraiText6:
	.db "ete tue..",0
KuraiText7:
	.db "Quoi? Un soldat",0
KuraiText8:
	.db "d'Xtech?!",0
KuraiText9:
	.db "Que se passe-t-il?",0
KuraiText10:
	.db "Je dois aller a",0
KuraiText11:
	.db "Kiata.",0
KuraiText12:
	.db "Ils sauront ce",0
KuraiText13:
	.db "qui se passe.",0
KuraiText14:
	.db "Le sol",0
KuraiText15:
	.db "est bizzare..",0
KuraiText16:
	.db "Desole, mais j'ai",0
KuraiText17:
	.db "assez d'ennuis",0
KuraiText19:
	.db "Fuck!",0
KuraiText20:
	.db "Ou est-elle?",0
KuraiText21:
	.db "Je veux juste",0
KuraiText22:
	.db "Akara...",0
KuraiText23:
	.db "Je ne suis pas",0
KuraiText24:
	.db "ton sauveur.",0
KuraiText25:
	.db "Akara, je",0
KuraiText26:
	.db "viens..",0
KuraiText27:
	.db "Fuck! c'est",0
KuraiText28:
	.db "le temps de la",0
KuraiText29:
	.db "chasse aux bandits.",0
KuraiText30:
	.db "Allons a la",0
KuraiText31:
	.db "tour.",0
ManText1:
	.db "Kiata est passe la",0
ManText2:
	.db "grotte a l'ouest. Bloque.",0
ManText3:
	.db "L'homme avec la cle",0
ManText4:
	.db "etait un diplomate... il est",0
ManText5:
	.db "mort dans la prison du chateau.",0
ManText6:
	.db "Eh! Tu est ce tueur???",0
ManText7:
	.db "Le tue-t-on?",0
ManText8:
	.db "Gardes! Arretez-le!",0
ManText9:
	.db "Oui, on sait..",0
ManText10:
	.db "Un sorcier dans la tour",0
ManText11:
	.db "les a tue.",0
ManText12:
	.db "Nous aiderez-vous?",0
ManText14:
	.db "Sinon ce monde est fini.",0
ManText16:
	.db "Kurai...",0
ManText17:
	.db "ils ont eu Akara.",0
ManText18:
	.db "Dans la tour...une",0
ManText20:
	.db "force l'entoure.",0
ManText21:
	.db "Vous devez avoir l'epee de foi.",0
ManText23:
	.db "Cherchez la grotte",0
ManText25:
	.db "du sud-ouest. Le sage la",0
ManText26:
	.db "sait ou elle est.",0
BanditsText1:
	.db "l'épée de foi!",0
BanditsText2:
	.db "Des bandits vous entourent!",0
BanditsText3:
	.db "Ils volent",0
manswitch:
	.db "Il y a une switche secrete dans",0
BanditCastle:
	.db "Le chateau des bandits..",0
BanditsText4:
	.db "Le chef des bandits!",0
BanditsText5:
	.db "Il a",0
force:
	.db "Tu est repousse par des forces",0
NewGame:
	.db "3. Debut",0
SaveGame:
	.db "1. Save 1     2. Save 2",0

Saved:
	.db "Sauve",0
Loaded:
	.db "Charge",0

YouDodge:
	.db "You dodge",0

ThankYou:
	.db "Merci",0

WeaponShop:
	.db "Epee:",0
ArmorShop:
	.db "Armure:",0
willcost:
	.db "Cout:",0
WAset1:
	.db "1. Bois",0
WAset2:
	.db "2. Fer",0
WAset3:
	.db "3. Feu",0
WAset4:
	.db "4. Magi",0
WAset5:
	.db "5. Dragon",0
Restored:
	.db "Soigne",0
Inn:
	.db "Bienvenue",0
OneNight:
	.db "Une Nuit:",0

ItemShop:
	.db "Item",0
ItemStore:
	.db "3. Bombe",0
ItemMenu:
	.db "1. KitMedi    ",0
	.db "2. KitMagi",0
DELtoExit:
	.db "DEL: Sortir",0
YouFind:
	.db "Tu trouve..",0
scroll:
	.db "un livre. XP",0
Youknowit:
	.db "Connu",0
ChooseSpells:
	.db "Choix Sort",0
ChooseSkills:
	.db "Choix Hab.",0
Allocate:
	.db "Allouer:",0
SlashAll:
	.db "Couper",0
Regeneration:
	.db "Regen",0
HPMP:
	.db "HP<->MP",0
Smoke:
	.db "Fume",0	;Note: Fume = Fumee
ReLife:
	.db "ReVie",0
Build:
	.db "Batir",0
Xfrm:
	.db "Xfrm",0
Bribe:
	.db "Voler",0
MedKit:
	.db "KitHP",0
MagKit:
	.db "KitMP",0
ArcBolt: 
	.db "Bombe",0
EnemyAttacks:
	.db "attaque!!",0
damage:
	.db "dmg!",0
BlankText:
	.db "                    ",0
Victory:
	.db "Victoire!",0
xp:
	.db "XP:",0
gold:
	.db "Or:",0
ToNextLevel:
	.db "Prochain:",0
GameOVER:
	.db "Game Over",0
YourMP:
	.db "M:",0
Escape:
	.db "Fuite",0
Failed:
	.db "manquee..",0
HPstr:
	.db "HP:",0
MPstr:
	.db "MP:",0
Strength:
	.db "AT:",0
Intelligence:
	.db "INT:",0
Speed:
	.db "DX:",0
Heal:
	.db "Cure",0
Burn:
	.db "Feu",0
Star:
	.db "Astre",0
Rage:
	.db "Rage",0
Sire:
	.db "Foi",0
Rock:
	.db "Roche",0
Haste:
	.db "Hate",0
Doom:
	.db "Mort",0
needmp:
	.db "Pas de MP!" ,0
somegold:
	.db "de l'or",0
Stats:
	.db "Statut",0
Level:
	.db "Lvl:",0
Search:
	.db "..rien",0
yesorno:
	.db "1. Oui      ",0
	.db "2. Non",0

StatusScreen:
;Statut
;Objets
;Fouiller
;Magie
 .db 4,1,28
 .db %01111111, %11111111, %11111111, %11111110
 .db %11000000, %00000000, %00000000, %00000011
 .db %10000000, %01110100, %00000100, %00000001
 .db %10000000, %01000110, %01101110, %00000001
 .db %10000000, %01100100, %10100100, %00000001
 .db %10000000, %01000100, %10100100, %00000001
 .db %10000000, %01110010, %01100010, %00000001
 .db %10000000, %00000000, %00000000, %00000001
 .db %10000010, %01000001, %00000010, %00000001
 .db %10000101, %01000000, %00100111, %00110001
 .db %10000101, %01100001, %01010010, %01100001
 .db %10000101, %01010101, %01100010, %00010001
 .db %10000010, %01100010, %00110001, %01100001
 .db %10000000, %00000000, %00000000, %00000001
 .db %10001110, %00000001, %01010000, %00000001
 .db %10001000, %10010100, %01010010, %01010001
 .db %10001101, %01010101, %01010101, %01100001
 .db %10001001, %01010101, %01010110, %01000001
 .db %10001000, %10011101, %01010011, %01000001
 .db %10000000, %00000000, %00000000, %00000001
 .db %10000001, %00010000, %00100100, %00000001
 .db %10000001, %10110011, %01010000, %10000001
 .db %10000001, %01010101, %00110101, %01000001
 .db %10000001, %00010101, %00010101, %10000001
 .db %10000001, %00010011, %00100100, %11000001
 .db %10000000, %00000000, %00000000, %00000001
 .db %11000000, %00000000, %00000000, %00000011
 .db %01111111, %11111111, %11111111, %11111110
