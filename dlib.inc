Routine1:
	push hl
	ld hl,KuraiPortrait
	ld bc,$3C26
	call NASRWARP	
	pop hl
	ld (DUMMY1),hl
	ld hl,Allocate
	ld de,$080A
	call DISPSTRING
	call DISPLAYSTATS	
	ld bc,$130B
	ld hl,ALLOCATER
	call drawit2
;FROMADD:
	ld hl,BlankText
	ld de,$082A
	call DISPSTRING
	ld hl,(DUMMY1)
	dec hl
	dec hl

	ld de,$082A
	call DISPHL
	call DRAWSTATS
;ALLOCLOOP:	
	bcall(_getcsc)
    cp G_RIGHT
	jp z,TEMPSPACE+138
	cp G_DOWN
	jp z,TEMPSPACE+107
    cp G_UP
	jp nz,TEMPSPACE+58
;ALLOCUP:
	ld a,(BKY)
	cp $13
	jp z,TEMPSPACE+58	
	ld hl,ALLOCATER
	call drawit
	ld a,(BKY)
	sub 6
	ld (BKY),a
	ld hl,ALLOCATER
	call drawit
	jp TEMPSPACE+58	
;ALLOCDOWN:
	ld a,(BKY)
	cp $2B
	jp z,TEMPSPACE+58	
	ld hl,ALLOCATER
	call drawit
	ld a,(BKY)
	add a,6
	ld (BKY),a
	ld hl,ALLOCATER
	call drawit
	jp TEMPSPACE+58	

;ADDPOINT:
	ld hl,(DUMMY1)
	dec hl
	dec hl
	ld de,0
	and a
	sbc hl,de
	jp z,TEMPSPACE+231 ;DoneAllocate
	ld (DUMMY1),hl
	ld a,(BKY)
	cp $13
	call z,TEMPSPACE+186
	cp $19
	call z,TEMPSPACE+204
	cp $1F
	call z,TEMPSPACE+195
	cp $25
	call z,TEMPSPACE+213
	cp $2B
	call z,TEMPSPACE+222
	jp  TEMPSPACE+35

;ADDHP:
	ld hl,(MAXHP)
	inc hl
	inc hl
	ld (MAXHP),hl
	ret

;ADDST:
	ld hl,(STRENGTH)
	inc hl
	inc hl
	ld (STRENGTH),hl
	ret

;ADDMP:
	ld hl,(MAXMP)
	inc hl
	inc hl
	ld (MAXMP),hl
	ret

;ADDINT:
	ld hl,(INTELLIGENCE)
	inc hl
	inc hl
	ld (INTELLIGENCE),hl
	ret	

;ADDSPEED:	
	ld hl,(SPEED)
	inc hl
	inc hl
	ld (SPEED),hl
	ret

;DONEALLOCATE:
	ld hl,(MAXMP)
	ld de,(MAXHP)
	ld (MP),hl
	ld (HP),de
	ld hl,(STRENGTH)
	ld (STRENGTHMAX),hl
	ld hl,(DEFENSE)
	ld (DEFENSEMAX),hl
	ld a,(KURAILEVEL)
	cp 8
	jp nc,TEMPSPACE+271
	ld de,80
	jp TEMPSPACE+274

;NOW160:
	ld de,160
;REGULARCONT:
	ld hl,0	
;LVLADD:	
	add hl,de
	dec a
	jp nz,TEMPSPACE+277
	ld de,50
	add hl,de
	ld de,(TOLEVEL)
	add hl,de
	ld (TOLEVEL),hl
;SKILLSPELLS:
	call EFFECT
	call DRAWWALLS
	call DELEXIT
	ld hl,ChooseSpells
	ld de,$0630
	call DISPSTRING
	ld bc,$060B
	ld hl,ALLOCATER
	call drawit2
	ld a,1
	ld (DUMMY1),a

	ld hl,Heal
	ld de,$0515
	call DISPSTRING
	ld a,$06
	ld (DUMMY2),a
	ld hl,(INTELLIGENCE)
	ld de,12
	and a
	sbc hl,de
	jp c,TEMPSPACE+522
	ld hl,Burn
	ld de,$0B15
	call DISPSTRING
	ld a,$0C
	ld (DUMMY2),a
	ld hl,(INTELLIGENCE)
	ld de,25
	and a
	sbc hl,de
	jp c,TEMPSPACE+522
	ld hl,Star
	ld de,$1115
	call DISPSTRING
	ld a,$12
	ld (DUMMY2),a
	ld hl,(INTELLIGENCE)
	ld de,34
	and a
	sbc hl,de
	jp c,TEMPSPACE+522	
	ld hl,Rage
	ld de,$1715
	call DISPSTRING
	ld a,$18
	ld (DUMMY2),a
	ld hl,(INTELLIGENCE)
	ld de,56
	and a
	sbc hl,de
	jp c,TEMPSPACE+522	
	ld hl,Sire
	ld de,$1D15
	call DISPSTRING
	ld a,$1E
	ld (DUMMY2),a
	ld hl,(INTELLIGENCE)
	ld de,70
	and a
	sbc hl,de
	jp c,TEMPSPACE+522	
	ld hl,Rock
	ld de,$2315
	call DISPSTRING
	ld a,$24
	ld (DUMMY2),a
	ld hl,(INTELLIGENCE)
	ld de,90
	and a
	sbc hl,de
	jp c,TEMPSPACE+522	
	ld hl,Haste
	ld de,$2915
	call DISPSTRING
	ld a,$2A
	ld (DUMMY2),a
	ld hl,(INTELLIGENCE)
	ld de,99
	and a
	sbc hl,de
	jp c,TEMPSPACE+522	
	ld hl,Doom
	ld de,$2F15
	call DISPSTRING
	ld a,$30
	ld (DUMMY2),a
;STARTADD2:              ;522
	ld hl,WhichSpells
;STARTLOOP2:
	push hl
;STARTLOOP:
	bcall(_getcsc)
    cp G_RIGHT
	jp z,TEMPSPACE+634
	cp G_DOWN
	jp z,TEMPSPACE+590
	cp G_DEL
	jp z,TEMPSPACE+651
    cp G_UP
	jp nz,TEMPSPACE+526	
;STARTUP:
	ld a,(BKY)
	cp $06
	jp z,TEMPSPACE+526
	ld hl,ALLOCATER
	call drawit
	pop hl
	dec hl
	push hl
	ld a,(DUMMY1)
	dec a
	ld (DUMMY1),a
	ld a,(BKY)
	sub 6
	ld (BKY),a
	ld hl,ALLOCATER
	call drawit
	jp TEMPSPACE+526	
;STARTDOWN:
	ld a,(DUMMY2)
	ld b,a
	ld a,(BKY)
	cp b
	jp z,TEMPSPACE+526	
	ld hl,ALLOCATER
	call drawit
	pop hl
	inc hl
	push hl
	ld a,(DUMMY1)
	inc a
	ld (DUMMY1),a
	ld a,(BKY)
	add a,6
	ld (BKY),a
	ld hl,ALLOCATER
	call drawit
	jp TEMPSPACE+526
	
;STARTADD:
	pop hl
	ld a,(DUMMY1)
	ld b,a
	ld a,(hl)
	cp b
	jp z,TEMPSPACE+655
	ld a,(DUMMY1)
	ld (hl),a		
	jp BEGIN2

;BEGINSKILL:
	pop hl
;; begin adding skill
	jp BEGIN2

;ALREADYKNOW:
	push hl
	ld hl,Youknowit
	call DISPSTRING2
	call ERASETEXT2
	pop hl
	jp  TEMPSPACE+525

Routine1_2:
	call DRAWWALLS
	call DELEXIT
	ld hl,ChooseSkills
	ld de,$0630
	call DISPSTRING
	ld bc,$060B
	ld hl,ALLOCATER
	call drawit2

	ld hl,SlashAll
	ld de,$0515
	call DISPSTRING
		;ld hl,Regeneration
		;ld de,$0B15
	ld d,$0B
	call DISPSTRING
		;ld hl,HPMP
		;ld de,$1115
	ld d,$11
	call DISPSTRING
		;ld hl,Smoke
		;ld de,$1715
	ld d,$17
	call DISPSTRING
		;ld hl,ReLife
		;ld de,$1D15
	ld d,$1D
	call DISPSTRING
		;ld hl,Build
		;ld de,$2315
	ld d,$23
	call DISPSTRING
		;ld hl,Xfrm
		;ld de,$2915
	ld d,$29
	call DISPSTRING
		;ld hl,Bribe
		;ld de,$2F15
	ld d,$2F
	call DISPSTRING

;ADDSKILL2:
	ld hl,WhichSkills
;STARTADD3:
	push hl
;ADDLOOP:          ;72
	bcall(_getcsc)
    cp G_RIGHT
	jp z,TEMPSPACE+163
	cp G_DOWN
	jp z,TEMPSPACE+129	
	cp G_DEL
	jp z,TEMPSPACE+173	
    cp G_UP
	jp nz,TEMPSPACE+72	
;ADDUP:
	ld a,(BKY)
	cp $06
	jp z,TEMPSPACE+72	
	ld hl,ALLOCATER
	call drawit
	pop hl
	dec hl
	push hl
	ld a,(BKY)
	sub 6
	ld (BKY),a
	ld hl,ALLOCATER
	call drawit
	jp TEMPSPACE+72	
	
;ADDDOWN:
	ld a,(BKY)
	cp $30
	jp z,TEMPSPACE+72		
	ld hl,ALLOCATER
	call drawit
	pop hl
	inc hl
	push hl
	ld a,(BKY)
	add a,6
	ld (BKY),a
	ld hl,ALLOCATER
	call drawit
	jp TEMPSPACE+72	
	
;SKILLADD:
	pop hl
	ld a,(hl)
	cp 1
	jp z,TEMPSPACE+175	
	ld (hl),1		
	push hl
;NOSKILL:
	pop hl
	ret


;ALREADYKNOW2:
	push hl
	ld hl,Youknowit
	call DISPSTRING2
	call ERASETEXT2
	pop hl
	jp  TEMPSPACE+71	

Routine2:
	call DELAY2
	ld hl,StatusScreen
	ld bc,$3C3C
	call NASRWARP	
	ld bc,$0540
	ld (BKX),bc
	call DRAWWIDE
;STATLOOP:          ;22
 	ld a,$3E
 	out (1),a
 	in a,(1)
	bit 7,a
	jp z,RETFROMSTATS2
	bit 5,a ; 2ND
 	jp z,TEMPSPACE+130
 	bit 0,a
 	jp z,TEMPSPACE+89
	bit 3,a
	jp nz,TEMPSPACE+22
;STATUP:	
	call DELAY
	call DRAWWIDE
	ld a,(BKY)
	cp $05
	jp nz,TEMPSPACE+75
	ld bc,$1740
	ld (BKX),bc
	call DRAWWIDE
	jp TEMPSPACE+22

;CONTUP:
	ld a,(BKY)
	sub 6
	ld (BKY),a
	call DRAWWIDE
	jp TEMPSPACE+22
	
;STATDOWN:
	call DELAY
	call DRAWWIDE
	ld a,(BKY)
	cp $17
	jp nz,TEMPSPACE+116
	ld bc,$0540
	ld (BKX),bc
	call DRAWWIDE
	jp TEMPSPACE+22

;CONTDOWN:
	ld a,(BKY)
	add a,6
	ld (BKY),a
	call DRAWWIDE
	jp TEMPSPACE+22

;STATPRESS:
	ld a,(BKY)
	cp $11
	jp z,TEMPSPACE+361
	call DRAWWALLS
	ld hl,KuraiPortrait
	ld bc,$3C24
	call NASRWARP	
	ld a,(BKY)
	cp $17
	jp z,TEMPSPACE+660
	cp $0B
	jp z,TEMPSPACE+582
;STATS:
	ld hl,Stats
	ld de,$0306
	call DISPSTRING
	ld hl,Level
	ld de,$0C14
	call DISPSTRING
	ld a,(KURAILEVEL)
	ld l,a
	ld h,0
	ld de,$0C22
	call DISPHL
	ld bc,$333C
        	ld hl,Logo ;; displays kurai
        	call NASRWARP

	call DISPLAYSTATS
	call DRAWSTATS2
	ld hl,(HP)	
	ld de,$1222
	call DISPHL
	ld hl,(MP)	
	ld de,$1822
	call DISPHL
	ld hl,xp
	ld de,$3017
	call DISPSTRING
	ld hl,(XP)	
	ld de,$3022
	call DISPHL
	ld hl,(GOLD)	
	ld de,$3622
	call DISPHL
	ld hl,gold
	ld de,GOLD_STATS_COORD
	call DISPSTRING3 ;; only so i dont have to add call WaitKey
	call DRAWWALLS
	ld hl,KuraiPortrait
	ld bc,$3C24
	call NASRWARP	
	ld bc,$333C
    ld hl,Logo ;; displays kurai
    call NASRWARP
	
	ld hl,WeaponShop
	ld de,WEAPON_COORD
	call DISPSTRING
	ld de,ARMOR_COORD
	call DISPSTRING

	ld hl,(WEAPON)	
	ld de,$1222
	call DISPHL
	ld hl,(DEFENSE)	
	ld de,$1822
	call DISPHL

	ld hl,MedKit
	ld de,$1E0F
	call DISPSTRING
	ld de,$240D
	call DISPSTRING

	ld hl,WhichItems
	ld a,(hl)
	ld l,a
	ld h,0
	ld de,$1E22
	call DISPHL
	ld hl,WhichItems
	inc hl
	ld a,(hl)
	ld l,a
	ld h,0
	ld de,$2422
	call DISPHL
	jp RETFROMSTATS

;SEARCHFOR:
	ld hl,YouFind
	ld de,$060B
	call DISPSTRING
	call DELAY3	
	call DELAY3	
	ld a,(DUNGEONCHECK)
	or a
	jp nz,TEMPSPACE+447
;CHECKPLACE:
	ld a,(SCENARIO)
	cp 9
	jp c,TEMPSPACE+447
	cp 10
	jp z,TEMPSPACE+404
	ld hl,MAP4+533
	ld (hl),6
	jp TEMPSPACE+409
;NOCHEST:
	ld hl,MAP4+533
	ld (hl),0
;YESCHEST:
	ld hl,(X)	
	ld de,$0020
	and a
	sbc hl,de
	jp nz,TEMPSPACE+460
	call DRAWWALLS
	ld de,$1C1E
	ld hl,acave
	call DISPSTRING3
	call ERASESCREEN
	LD DE,711
	ld bc,$131B
	ld a,-2
	jp MAP4PLACER2	

;NOCHECKPLACE:        ;447
	ld a,(SCENARIO)
	cp 3
	jp z,TEMPSPACE+538
	cp 8
	jp nc,TEMPSPACE+472
;FINDNOTHING:
	ld hl,Search
	ld de,$0C0B
	call DISPSTRING

;RETFROMSTATS9:
	jp RETFROMSTATS

;BANDITSEARCH:
	ld a,(DUNGEONCHECK)
	cp -1
	jp z,TEMPSPACE+509
	ld hl,(X)	
	ld de,$1002
	and a
	sbc hl,de
	jp nz,TEMPSPACE+460
	ld hl,aswitch
	ld de,$0C0B
	call DISPSTRING
	ld hl,MAP3+653
	ld (hl),0
	jp TEMPSPACE+469		
;LASTSEARCH:
	ld hl,(X)	
	ld de,$0E14
	and a
	sbc hl,de
	jp nz,TEMPSPACE+460
	ld hl,aswitch
	ld de,$0C0B
	call DISPSTRING
	ld hl,MAP4+597
	ld (hl),0
	jp TEMPSPACE+469	
	
;DUNGEON1SEARCH:  ;538
	ld hl,(X)	
	ld de,$051C
	and a
	sbc hl,de
	jp nz,TEMPSPACE+460
	ld hl,thekey
	ld de,$0C0B
	call DISPSTRING3
	call DRAWWALLS
		;ld hl,corpse
	ld de,$1C13
	call DISPSTRING3
		;ld hl,mykey
	ld de,$2415
	call DISPSTRING3
	ld a,4
	ld (SCENARIO),a
	jp LEADER2

;ITEMS:
	ld hl,ItemMenu
	ld de,$0505
	call DISPSTRING	
	ld de,$3410
	call DISPSTRING
	call ITEMHEALS
;RETITEM:
	call ITEMHEALS2
	ld hl,WhichItems
	push hl
;USEITEMLOOP:   ;607
	bcall(_getcsc)
    cp G_1
    jp z,TEMPSPACE+643
    cp G_DEL
    jp z,TEMPSPACE+735
    cp G_2
    jp nz,TEMPSPACE+607
;USEMAGKIT:
	pop hl
	inc hl
	ld a,(hl)
	or a
	jp z,TEMPSPACE+600
	dec a
	ld (hl),a
	ld de,40
	call OVERMAXMP
	jp TEMPSPACE+600

;USEMEDKIT:
	pop hl
	ld a,(hl)
	or a
	jp z,TEMPSPACE+600
	dec a
	ld (hl),a
	ld de,90
	call OVERMAXHP
	jp TEMPSPACE+600
	
;HEALING:   ;660
	call ITEMHEALS
	call YESNO
	ld hl,willcost
	ld de,$2005
	call DISPSTRING	
;REHEAL:
            	LD de,$1822
            	ld hl,BlankText
				push hl
            	call DISPSTRING
				pop hl
            	LD de,$2016
            	call DISPSTRING
;DIDNTHEAL:
	ld de,(HP)
	ld hl,(MAXHP)
	and a
	sbc hl,de
	srl h
	rr l
	srl h
	rr l
	push hl
	ld de,$2016
	call DISPHL
	call ITEMHEALS2
;HEALINGLOOP:
	bcall(_getcsc)
    cp G_1
	jp z,TEMPSPACE+742          
    cp G_DEL
	jp z,TEMPSPACE+738
    cp G_2
	jp nz,TEMPSPACE+720
;ENDHEAL:
	pop hl
	jp  RETFROMSTATS2


;USEHEAL:
	pop hl
	ex de,hl
	ld hl,(MP)
	and a
	sbc hl,de
	jp c,TEMPSPACE+692
	ld (MP),hl
	ld hl,(MAXHP)	
	ld (HP),hl
	jp TEMPSPACE+675

Routine3:
	ld a,(DUNGEONCHECK)
	cp 8
	jp c,CASTLE	

	call DRAWWALLS
	ld hl,gold
	ld de,$360F
	call DISPSTRING
	ld hl,(GOLD)	
	ld e,$22
	call DISPHL
	call DELEXIT
	ld de,$0505
	ld bc,(X)
	ld a,c
	cp 12 
	jp z,ITEMSHOP
	cp 18
	jp z,TEMPSPACE+416
	cp 14
	jp z,TEMPSPACE+207
	cp 19
	jp z,DEADMAN

;WEAPONSHOP:
	ld hl,WeaponShop
	call DISPSTRING
	call TEMPSPACE+619
	ld hl,180
	ld de,$0E40
	call DISPHL
	ld hl,700
	ld de,$1440
	call DISPHL
	ld hl,3400
	ld de,$1A40
	call DISPHL
	ld hl,8000
	ld de,$2040
	call DISPHL
	ld hl,12000
	ld de,$2640
	call DISPHL
;WPNLOOP:            ;113
	bcall(_getcsc)
	cp G_2
	jp z,TEMPSPACE+155
	cp G_3
	jp z,TEMPSPACE+164
	cp G_4
	jp z,TEMPSPACE+173
	cp G_5
	jp z,TEMPSPACE+182
	cp G_DEL
	jp z,RETFROMSTATS2
	cp G_1
	jp nz,TEMPSPACE+113
;WPN1:
	ld de,180
	ld bc,15
	jp TEMPSPACE+188
;WPN2:
	ld de,700
	ld bc,45
	jp TEMPSPACE+188
;WPN3:
	ld de,3400
	ld bc,90
	jp TEMPSPACE+188
;WPN4:
	ld de,8000
	ld bc,120
	jp TEMPSPACE+188
;WPN5:
	ld de,12000
	ld bc,220
;WPNDONE:  ;188
	ld hl,(GOLD)
	and a
	sbc hl,de
	jp c,TEMPSPACE+113
	ld (GOLD),hl
	ld (WEAPON),bc
	jp THANKYOU

;ARMORSHOP:  ;207
	ld hl,ArmorShop
	call DISPSTRING
	call TEMPSPACE+619
	ld hl,220
	ld de,$0E40
	call DISPHL
	ld hl,700
	ld de,$1440
	call DISPHL
	ld hl,2500
	ld de,$1A40
	call DISPHL
	ld hl,5666
	ld de,$2040
	call DISPHL
	ld hl,9999
	ld de,$2640
	call DISPHL
;ARMRLOOP:           ;261
	bcall(_getcsc)
	cp G_2
	jp z,TEMPSPACE+303
	cp G_3
	jp z,TEMPSPACE+312
	cp G_4
	jp z,TEMPSPACE+321
	cp G_5
	jp z,TEMPSPACE+330
	cp G_DEL
	jp z,RETFROMSTATS2
	cp G_1
	jp nz,TEMPSPACE+261
;ARM1:
	ld de,220
	ld bc,20
	jp TEMPSPACE+336
;ARM2:
	ld de,700
	ld bc,41
	jp TEMPSPACE+336
;ARM3:
	ld de,2500
	ld bc,65
	jp TEMPSPACE+336
;ARM4:
	ld de,5666
	ld bc,91
	jp TEMPSPACE+336
;ARM5:
	ld de,9999
	ld bc,120
;ARMRDONE:
	ld hl,(GOLD)
	and a
	sbc hl,de
	jp c,TEMPSPACE+261
	ld (GOLD),hl
	ld (DEFENSE),bc
	jp THANKYOU

;OLDMAN:  ;; 355 
	call DRAWWALLS
	ld a,(SCENARIO)
	cp 6
	jp nc,TEMPSPACE+404
	ld de,$1004
	ld hl,OldManText1
	call DISPSTRING	
	ld d,$16
	call DISPSTRING	
	ld d,$1D
	call DISPSTRING	
	ld d,$24
	call DISPSTRING	
	call TALK8
	call TALK9
	ld a,6
	ld (SCENARIO),a
	jp RETFROMSTATS2

;NOTHINGMANSAYS:
	ld de,$1C10
	ld hl,saysnothing
	call DISPSTRING3
	jp RETFROMSTATS2

;INN:  ;416
	ld a,(DUNGEONCHECK)
	cp 10
	jp z,TEMPSPACE+355
	ld hl,Inn
	ld de,$1213
	call DISPSTRING
	ld d,$1A
	call DISPSTRING
	call ADD20LOOP
	ld (DUMMY1),hl
	ld de,$1A35
	call DISPHL
	call YESNO
;INNLOOP:        ;453
	bcall(_getcsc)
    cp G_2
    jp z,TEMPSPACE+521
	cp G_DEL
	jp z,TEMPSPACE+521     
    cp G_1
	jp nz,TEMPSPACE+453
;YESINN:	
	ld de,(DUMMY1)
	ld hl,(GOLD)
	and a
	sbc hl,de
	jp c,TEMPSPACE+453
	ld (GOLD),hl
	ld a,40
;INNFLASHLOOP:
	push af
	call INVERTSCREEN
	pop af
	dec a
	jp nz,TEMPSPACE+489

	ld hl,Restored
	ld de,$2620
	call DISPSTRING
	ld hl,(MAXHP)
	ld de,(MAXMP)
	ld (HP),hl
	ld (MP),de	

;SAVENOINN:		
	call DRAWWALLS
	ld hl,SaveGame
	ld de,$0505
	call DISPSTRING	
	call DELEXIT
;SAVELOOP:         ;536
	bcall(_getcsc)
    cp G_2
	jp z,TEMPSPACE+569     
	cp G_DEL
	jp z,RETFROMSTATS2
    cp G_1
    jp nz,TEMPSPACE+536
;SAVEGAME1:
	ld hl,SaveSSI
	ld (DUMMY1),hl
    LD DE,SAVERAM
	ld hl,scenario1
	jp TEMPSPACE+581
;SAVEGAME2:	
	ld hl,SaveSSI2
	ld (DUMMY1),hl
        	LD DE,SAVERAM2
	ld hl,scenario2
;SAVEGAME:
	push hl
        	LD HL,TOLEVEL
        	LD BC,28
        	LDIR
	ld de,(DUMMY1)
	ld hl,WhichSpells
 	ld bc,24
 	ldir
	pop hl
	ld a,(SCENARIO)
	ld (hl),a
	ld hl,Saved
	ld de,$2120
	call DISPSTRING
	jp THANX

;DISPWPNSET1:
	ld hl,willcost
	ld de,$0540
	call DISPSTRING
		;ld hl,WAset1
	ld de,$0E0A
	call DISPSTRING	
		;ld hl,WAset2
		;ld de,$140A
	ld d,$14
	call DISPSTRING	
		;ld hl,WAset3
		;ld de,$1A0A
	ld d,$1A
	call DISPSTRING	
		;ld hl,WAset4
		;ld de,$200A
	ld d,$20
	call DISPSTRING	
		;ld hl,WAset5
		;ld de,$260A
	ld d,$26
	jp DISPSTRING	

	
Routine3_2:
	call DRAWWALLS
	ld de,$0D0B
	ld a,(SCENARIO)
	cp 5
	jp nc,TEMPSPACE+121
	ld hl,ManText1
	call DISPSTRING	
	ld d,$14
	call DISPSTRING	
	ld d,$1B
	call DISPSTRING	
	ld d,$22
	call DISPSTRING	
	ld d,$29
	call DISPSTRING3
	call DRAWWALLS
	ld de,$1C09
	call DISPSTRING3
	call YESNO
	ld de,$1114
	ld hl,ManText7
	call DISPSTRING	
	ld hl,MAP2+814
	ld (hl),0
	ld a,1	
	ld (NUMBEROFENEMIES),a
	ld a,3	
	ld (SCENARIO),a
;KILLMANLOOP:   ;76
	bcall(_getcsc)
    cp G_1
    jp z,GIMPMAN
    cp G_2
	jp nz,TEMPSPACE+76
;FLEEMAN:
	ld de,$260A
	ld hl,ManText8
	call DISPSTRING3
	call ERASESCREEN
	jp  LEADER1TOWN ;; yay! you get to fight an xtech soldier! =)))

;ABOUTSWITCH:
	ld de,$1A07
	ld hl,manswitch
	call DISPSTRING	
	ld d,$21
	call DISPSTRING
	jp RETFROMSTATS

;MAN2:
	ld a,(SCENARIO)
	cp 9
	jp nc,TEMPSPACE+230
	cp 8
	jp nc,TEMPSPACE+104

	ld hl,ManText9
	call DISPSTRING	
	ld d,$0D
	call DISPSTRING	
	ld d,$14
	call DISPSTRING	
	ld d,$1B
	call DISPSTRING	
	ld d,$22
	call DISPSTRING	
	call TALK6
	call DRAWWALLS

	ld de,$140B
	ld hl,ManText16
	call DISPSTRING	
	ld d,$1B
	call DISPSTRING	

	call TALK7
	call DRAWWALLS

	ld de,$140B
	ld hl,ManText18
	call DISPSTRING	
    ld d,$1B
	call DISPSTRING	
    ld d,$22
	call DISPSTRING3

	call DRAWWALLS

	ld de,$140B
	ld hl,ManText23
	call DISPSTRING	
	ld d,$1B
	call DISPSTRING	
	ld d,$22
	call DISPSTRING
	jp RETFROMSTATS

;NOTHINGMANSAYS2:
	ld de,$1C10
	ld hl,saysnothing
	call DISPSTRING3
	jp RETFROMSTATS2

Routine5:
	;; first erases menu so the spell menu can come up...same with skill/item
	call ERASEMENU	
	call DISPMP
	xor a
	ld (DUMMY1),a
	ld hl,WhichSkills
	ld a,(hl)
	or a	
    jp z,TEMPSPACE+29
	push hl
    ld hl,SlashAll
	ld de, $1F49                    ;Y=2C,X=1A
	call DISPSTRING
	pop hl
;NEXT1S:
	inc hl
	ld a,(hl)
	or a
	jp z,TEMPSPACE+46
	push hl
    ld hl,Regeneration
	ld de, $2649                    ;Y=2C,X=1A
	call DISPSTRING
	pop hl
;NEXT2S:
	inc hl
	ld a,(hl)
	or a
	jp z,TEMPSPACE+63
	push hl
             ld hl,HPMP
	ld de, $2D49                    ;Y=2C,X=1A
	call DISPSTRING	
	pop hl
;NEXT3S:
	inc hl
	ld a,(hl)
	or a
	jp z,TEMPSPACE+80
	push hl
             ld hl,Smoke
	ld de, $3449                    ;Y=2C,X=1A
	call DISPSTRING	
	pop hl

;SKILLSTART:     ;80
	ld bc,$1F3F
	ld hl,CURSORMAGIC
	call drawit2
	call DELAY2

	ld hl,WhichSkills
;SkillLoop2:    ;95
	push hl
;SkillLoop:     ;96
	ld a,$3E
 	out (1),a
 	in a,(1)
 	bit 5,a
 	jp z,TEMPSPACE+235
	bit 3,a
    jp z,TEMPSPACE+169
 	bit 7,a  ; del
 	jp z,GOBACK
	bit 1,a
	jp z,TEMPSPACE+206
	bit 2,a
	jp z,TEMPSPACE+220
	bit 0,a
    jp nz,TEMPSPACE+96
;SKILLDOWN:
	ld a,(BKY)
	cp $34
	jp z,TEMPSPACE+96
	pop hl
	inc hl
	push hl
	ld hl,CURSORMAGIC
	call drawit
	ld a,(BKY)
	add a,7
	ld (BKY),a
	ld hl,CURSORMAGIC
	call drawit
	call DELAY2
	jp TEMPSPACE+96
;SKILLUP:
	ld a,(BKY)
	cp $1F
	jp z,TEMPSPACE+96				;####################################################################MAY NEED INDIRECTION?
	pop hl
	dec hl
	push hl
	ld hl,CURSORMAGIC
	call drawit
	ld a,(BKY)
	sub 7
	ld (BKY),a
	ld hl,CURSORMAGIC
	call drawit
	call DELAY2
	jp TEMPSPACE+96
;SKILLLFT:
	ld a,(DUMMY1)		
	or a
	jp z,TEMPSPACE+96
	dec a
	ld (DUMMY1),a
	jp REDISP2
;SKILLRT:
	ld a,(DUMMY1)		
	cp 1
	jp z,TEMPSPACE+96
	inc a
	ld (DUMMY1),a
	jp REDISP2
;SKILLPRESS:     ;235
	pop hl
	ld a,(hl)
	or a	
	jp z,TEMPSPACE+95
	ld (Save),hl
	ld hl,(MP)
	ld a,(DUMMY1)
	or a
	jp nz,TEMPSPACE+275
	ld a,(BKY)
	cp $26
	jp z,TEMPSPACE+337
	cp $34
	jp z,TEMPSPACE+357
	cp $2D
	jp z,TEMPSPACE+379
	jp TEMPSPACE+296
;SKILLPRESS2:   ;275
	ld a,(BKY)
	cp $26
	jp z,TEMPSPACE+469
	cp $34
	jp z,TEMPSPACE+499
	cp $2D
	jp z,TEMPSPACE+489
	jp TEMPSPACE+449

;SLASHSKILL:
	ld de,12
	and a
	sbc hl,de
	jp c,TEMPSPACE+651
	ld (MP),hl
	call SWORDSWING
	call SLASH_ALL
	ld hl,(STRENGTH)
	ld de,(ENEMYDFD1)
	and a
	sbc hl,de
	jp c,TEMPSPACE+331
	ex de,hl
	jp TEMPSPACE+334

;ENEMYDFDSALL:
	ld de,1
;GOHERE:
	jp  MAGIDAMAGE

;REGENSKILL:
	ld de,25
	and a
	sbc hl,de
	jp c,TEMPSPACE+651
	call REGENLOOPER
	ld a,1
	ld (REGENCHECK),a
	jp  MAGIDONE

;SMOKESKILL:
	ld a,(LEADERCHECK)
	or a
	jp nz,TEMPSPACE+95
	ld de,8
	and a
	sbc hl,de
	jp c,TEMPSPACE+651
	ld (MP),hl
	jp  YOUFLEE

;HPMPSKILL:
	ld hl,HPMP
	ld de, $181E
	call DISPSTRING
	call BLANK1429
	call KURAIMAGIC1
	call INVERTSCREEN
	ld hl,$181C
	ld (BKX),hl
	ld a,24
;HPMPLOOP:
	push af
	ld hl,(BKX)
	ld a,l
	inc a
	ld l,a
	ld (BKX),hl
	ld hl,MPHPSWITCH
	call drawit
	call DELAY
	pop af
	dec a
	jp nz,TEMPSPACE+405	
	ld hl,(MP)
	ld de,(HP)
	ld (HP),hl
	ld (MP),de
	call OVERMAXMP	
	jp  MAGIDONE

;RELIFESKILL:   ;449
	ld de,50
	and a
	sbc hl,de
	jp c,TEMPSPACE+651
	ld (MP),hl
	ld a,1
	ld (RELIFE),a
	jp MAGIDONE

;BUILDSKILL:  ;469
	ld de,10
	and a
	sbc hl,de
	jp c,TEMPSPACE+651
	call REGENLOOPER
	ld a,1
	ld (BUILDUP),a
	jp MAGIDONE

;XFRMSKILL:  ;489
	ld a,(LEADERCHECK)
	or a
	jp nz,TEMPSPACE+95
	jp RESTARTBATTLE
	
;STEALSKILL: ;499
	ld de,13
	and a
	sbc hl,de
	jp c,TEMPSPACE+651
	ld (MP),hl
	call BLANK1429
        	ld bc,$111D
	ld (DUMMY1),bc
	ld a,12
;STEALLOOP:
	push af
	ld bc,(DUMMY1)
	ld a,c
	inc a
	ld c,a		
	ld (DUMMY1),bc 
        	ld hl,KuraiMagic1
        	call NASRWARP
	ld bc,(DUMMY1)
	pop af
	dec a
	jp nz,TEMPSPACE+523

	ld bc,$1129
	ld (DUMMY1),bc
	ld a,12
;STEALLOOP2:
	push af
	ld bc,(DUMMY1)
	ld a,c
	dec a
	ld c,a		
	ld (DUMMY1),bc 
        	ld hl,KuraiMagic1
        	call NASRWARP
	ld bc,(DUMMY1)
	pop af
	dec a
	jp nz,TEMPSPACE+559
	ld a,r
	srl a
	and 7   
	or a
	jp z,TEMPSPACE+607
	cp 3
	jp z,TEMPSPACE+607
	ld hl,Failed
	jp TEMPSPACE+639
;STEALSOMETHING:
	ld a,r
	srl a
	and 7   
	or a
	jp z,TEMPSPACE+629
;STOLEMED:
	ld hl,WhichItems
	ld a,(hl)
	inc a
	ld (hl),a
	ld hl,MedKit
	jp TEMPSPACE+639
;STOLEMAG:	
	ld hl,WhichItems
	inc hl
	ld a,(hl)
	inc a
	ld (hl),a
	ld hl,MagKit
;STEALDONE:
	ld de, $1632
	call DISPSTRING
	call DELAY3
	jp MAGIDONE

;NEEDMP2:
	ld hl,needmp
	ld de, $163E
	call DISPSTRING
	call ERASETEXT2
	call DISPMP
	ld hl,(Save)
	jp  TEMPSPACE+95

Routine6:
	;; first erases menu so the spell menu can come up...same with skill/item
	call ERASEMENU	
	call DISPMP
	xor a
	ld (DUMMY1),a

	ld hl,WhichSpells
	ld a,(hl)
	or a	
	jp z,TEMPSPACE+29
	push hl
             ld hl,Heal
	ld de, $1F49                    ;Y=2C,X=1A
	call DISPSTRING
	pop hl
;NEXT1:
	inc hl
	ld a,(hl)
	or a
	jp z,TEMPSPACE+46
	push hl
             ld hl,Burn
	ld de, $2649                    ;Y=2C,X=1A
	call DISPSTRING
	pop hl
;NEXT2:
	inc hl
	ld a,(hl)
	or a
	jp z,TEMPSPACE+63
	push hl
             ld hl,Star
	ld de, $2D49                    ;Y=2C,X=1A
	call DISPSTRING	
	pop hl
;NEXT3:
	inc hl
	ld a,(hl)
	or a
	jp z,TEMPSPACE+80
	push hl
             ld hl,Rage
	ld de, $3449                    ;Y=2C,X=1A
	call DISPSTRING	
	pop hl
;SPELLSTART:         ;80
	ld bc,$1F3F
	ld hl,CURSORMAGIC
	call drawit2
	call DELAY2
	ld hl,WhichSpells
;MagicL2:    ;95
	push hl
;MagicL1:     ;96
	ld a,$3E
 	out (1),a
 	in a,(1)
	bit 3,a
    jp z,TEMPSPACE+169
 	bit 7,a  ; del
 	jp z,GOBACK
	bit 1,a
	jp z,TEMPSPACE+206
	bit 2,a
	jp z,TEMPSPACE+220
 	bit 5,a
 	jp z,TEMPSPACE+235
	bit 0,a
	jp nz,TEMPSPACE+96
;MAGICDOWN:
	ld a,(BKY)
	cp $34
	jp z,TEMPSPACE+96
	pop hl
	inc hl
	push hl
	ld hl,CURSORMAGIC
	call drawit
	ld a,(BKY)
	add a,7
	ld (BKY),a
	ld hl,CURSORMAGIC
	call drawit
	call DELAY2
	jp TEMPSPACE+96
;MAGICUP:
	ld a,(BKY)
	cp $1F
	jp z,TEMPSPACE+96
	pop hl
	dec hl
	push hl
	ld hl,CURSORMAGIC
	call drawit
	ld a,(BKY)
	sub 7
	ld (BKY),a
	ld hl,CURSORMAGIC
	call drawit
	call DELAY2
	jp TEMPSPACE+96
;MAGICLEFT:
	ld a,(DUMMY1)		
	or a
	jp z,TEMPSPACE+96
	dec a
	ld (DUMMY1),a
	jp REDISP
;MAGICRIGHT:
	ld a,(DUMMY1)		
	cp 1
	jp z,TEMPSPACE+96
	inc a
	ld (DUMMY1),a
	jp REDISP
;MAGICPRESS: ;235
	pop hl
	ld a,(hl)
	or a	
	jp z,TEMPSPACE+95
	ld (Save),hl
	cp 1
	jp z,TEMPSPACE+284
	cp 2
	jp z,TEMPSPACE+319
	cp 3
	jp z,TEMPSPACE+376
	cp 4
	jp z,TEMPSPACE+406
	cp 5
	jp z,TEMPSPACE+457
	cp 6
	jp z,TEMPSPACE+522
	cp 7
	jp z,TEMPSPACE+590
	cp 8
	jp z,TEMPSPACE+651

;HEALSPELL:
	ld hl,(MP)
	ld de,5
	and a
	sbc hl,de
	jp c,TEMPSPACE+712
	call SPELLCAST
	call HEALLOOPER
	ld a,(OK)
	cp 234
	jp z,MAGIDONE	
	ld de,90
	call OVERMAXHP
	jp MAGIDONE

;BURNSPELL:
	ld hl,(MP)
	ld de,6
	and a
	sbc hl,de
	jp c,TEMPSPACE+712
	call SPELLCAST
	ld a,99
;BURNLOOP:
	push af
	ld a,r
	srl a
	and 50	   
	add a,15
	ld (BKX),a
	ld a,r
	srl a
	and 10   
	add a,4
	ld (BKY),a
	ld hl,ENEMYHIT1
	call drawit
	pop af
	dec a 
	jp nz,TEMPSPACE+336  
	ld de,37
	jp  MAGIDAMAGE

;STAR_SPELL:
	ld hl,(MP)
	ld de,20
	and a
	sbc hl,de
	jp c,TEMPSPACE+712
	call SPELLCAST
	call INVERTSCREEN
	call SLASH_ALL
	call INVERTSCREEN
	ld de,110
	jp  MAGIDAMAGE

;RAGESPELL:
	ld hl,(MP)
	ld de,24
	and a
	sbc hl,de
	jp c,TEMPSPACE+712
	call SPELLCAST
	ld a,20
;FLASHSCREEN:	
	push af
	call INVERTSCREEN
	pop af
	dec a
	jp nz,TEMPSPACE+423
	ld a,(KURAILEVEL)
	ld hl,0
;LEVEL_LOOP:
	ld de,10
	add hl,de
	dec a
	jp nz,TEMPSPACE+438

	ld de,(STRENGTHMAX)	
	add hl,de
	ld (STRENGTH),hl
	jp MAGIDONE	
;SIRESPELL:
	ld hl,(MP)
	ld de,32
	and a
	sbc hl,de
	jp c,TEMPSPACE+712
	call SPELLCAST
	ld a,6
;SIRELOOP:
	push af
	ld bc,$2B26
        	ld hl,SireSpell1
        	call NASRWARP
	call DELAY
	ld bc,$2B26
        	ld hl,SireSpell2
        	call NASRWARP
	call DELAY
	ld bc,$2B26
        	ld hl,SireSpell3
        	call NASRWARP
	call DELAY
	pop af
	dec a
	jp nz,TEMPSPACE+474
	ld de,190
	jp MAGIDAMAGE

;ROCKSPELL:   ;522
	ld hl,(MP)
	ld de,24
	and a
	sbc hl,de
	jp c,TEMPSPACE+712
	call SPELLCAST
	ld hl,$AFAF			;load battle screen
	ld	de,GRAPH_MEM
	ld	bc,768
	ldir
	call FastCopy 		; Copy graphbuf to LCD
	ld a,10
;FLASHSCREEN2:	
	push af
	call INVERTSCREEN
	call DELAY
	pop af
	dec a
	jp nz,TEMPSPACE+553
	ld a,(KURAILEVEL)
	ld hl,0
;LEVEL_LOOP2:
	ld de,10
	add hl,de
	dec a
	jp nz,TEMPSPACE+571
	ld de,(DEFENSEMAX)	
	add hl,de
	ld (DEFENSE),hl
	jp MAGIDONE	
;HASTESPELL:   ;590
	ld hl,(MP)
	ld de,75
	and a
	sbc hl,de
	jp c,TEMPSPACE+712
	call SPELLCAST
	ld bc,$141F
	ld (DUMMY1),bc
	ld a,4
;HASTELOOP:
	push af
	ld bc,(DUMMY1)
	ld a,b
	dec a
	ld b,a		
	ld (DUMMY1),bc 
	ld hl,Kurai
        	call NASRWARP
	call INVERTSCREEN
	call DELAY
	pop af
	dec a
	jp nz,TEMPSPACE+614
	ld a,66
	ld (DOUBLE),a
	jp MAGIDONE

;DOOMSPELL:  ;651
	ld hl,(MP)
	ld de,55
	and a
	sbc hl,de
	jp c,TEMPSPACE+712
	call SPELLCAST	
	ld a,60
;DOOMLOOP:
	push af
;GETRANDOM2:
	CALL RAND
	cp 90
	jp nc,TEMPSPACE+669
	ld b,a
;GETRANDOM:
	CALL RAND
	cp 46
	jp nc,TEMPSPACE+678
	add a,16
	ld c,a
	ld hl,DoomSpell
	call NASRWARP
	pop af
	dec a 
	jp nz,TEMPSPACE+668
	ld hl,1
	ld (HP),hl
	ld de,500	
    jp MAGIDAMAGE

;NEEDMP:  ;712
	ld hl,needmp
	ld de, $163E
	call DISPSTRING
	call ERASETEXT2
	call DISPMP
	ld hl,(Save)
	jp TEMPSPACE+95
