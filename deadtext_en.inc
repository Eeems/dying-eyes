healher:
	.db "Heal her?",0
choicetext1:
	.db "Akara lays on the ground",0
choicetext2:
	.db "dying. If you use all",0
choicetext3:
	.db "your life force, shell live..",0
choicetext4:
	.db "but you will die.",0
badend1:
	.db "A noble, gesture...",0
badend2:
	.db "you throw yourself to hell",0
badend3:
	.db "knowing youll see her soon",0
badend4:
	.db "..someday.",0
cruelplace:
	.db "in this cruel place your",0
cruelplace2:
	.db "voice above the maelstrom",0
endgame1:
	.db "Although",0
endgame2:
	.db "Akara was",0
endgame3:
	.db "your lover,",0
endgame4:
	.db "faithful",0
endgame5:	
	.db "and caring,",0
endgame6:	
	.db "sometimes",0
endgame7:
	.db "life is just",0
endgame8:
	.db "more important.",0
lasttext1:
	.db "You see Akara, held captive",0
lasttext2:
	.db "by The Demon Wizard.",0
lasttext3:
	.db "Akara!!!",0
lasttext4:
	.db "Kurai, help me!!!",0
lasttext5:
	.db "The Demon Wizard speaks:",0
lasttext6:
	.db "She belongs to me,",0
lasttext7:
	.db "in life...",0
lasttext8:	
	.db "...or in death!",0
lasttext9:
	.db "The Wizard strikes Akara",0
lasttext10:
	.db "to the ground and attacks!",0

Serpent1:
	.db "A serpent appears!",0
faitharmor:
	.db "the Faith Armor!",0
Serpent2:
	.db "Defeat me to get",0
last:
	.db "The final battle?",0
acave:
	.db "A hidden cave!",0

saysnothing:
	.db "The man says nothing.",0
aswitch:
	.db "a switch!",0
OldManText1:
	.db "Ha, some savior. But,",0
OldManText2:
	.db "if you want the Faith Sword",0
OldManText3:
	.db "its in the cave north. Heres",0
thekey:
	.db "the key!",0
corpse:
	.db "A corpse appears!",0
mykey:
	.db "my key...my key..",0
Locked:
	.db "It is locked",0
Awakened:
	.db "Awakened",0
KuraiText1:
	.db "Huh?",0
KuraiText2:
	.db "The cell door",0
KuraiText3:
	.db "is open..",0
KuraiText4:
	.db "All the",0
KuraiText5:
	.db "guards have",0
KuraiText6:
	.db "been slain..",0
KuraiText7:
	.db "What? A Xtech",0
KuraiText8:
	.db "soldier?!",0
KuraiText9:
	.db "Whats going on?",0
KuraiText10:
	.db "Must get to",0
KuraiText11:
	.db "Kiata.",0
KuraiText12:
	.db "Theyll know",0
KuraiText13:
	.db "whats up.",0
KuraiText14:
	.db "The ground",0
KuraiText15:
	.db "feels funny..",0
KuraiText16:
	.db "Sorry, got",0
KuraiText17:
	.db "enuff problems",0
KuraiText19:
	.db "Fuck!",0
KuraiText20:
	.db "Where is she?",0
KuraiText21:
	.db "I just want",0
KuraiText22:
	.db "Akara...",0
KuraiText23:
	.db "Im not your",0
KuraiText24:
	.db "damn savior.",0
KuraiText25:
	.db "Akara, im",0
KuraiText26:
	.db "coming..",0
KuraiText27:
	.db "Fuck! its",0
KuraiText28:
	.db "bandit hunting",0
KuraiText29:
	.db "time..",0
KuraiText30:
	.db "Time to go to",0
KuraiText31:
	.db "the tower",0
ManText1:
	.db "Kiata is past the",0
ManText2:
	.db "west cave. Its locked.",0
ManText3:
	.db "The man with a key was",0
ManText4:	
	.db "a diplomat...he died in",0
ManText5:
	.db "the Castle prison.",0
ManText6:
	.db "Wait..youre that killer!",0
ManText7:
	.db "Kill him?",0
ManText8:
	.db "Guards! Stop him!",0
ManText9:
	.db "Yeah, we know..",0
ManText10:
	.db "A wizard in the tower",0
ManText11:
	.db "killed them.",0
ManText12:
	.db "Wont you help us?",0
ManText14:
	.db "Else this land is doomed.",0
ManText16:
	.db "Kurai...",0
ManText17:
	.db "theyve got Akara.",0
ManText18:
	.db "In the tower...a",0
ManText20:
	.db "forcefield is around it.",0
ManText21:
	.db "You need the Faith Blade.",0
ManText23:
	.db "Search the SouthWest",0
ManText25:
	.db "cave. The sage there",0
ManText26:
	.db "knows where it is.",0
BanditsText1:
	.db "the Faith Sword!",0
BanditsText2:
	.db "Bandits ambush you!",0
BanditsText3: 
	.db "They steal",0
manswitch:
	.db "I hear a secret switch is in",0
BanditCastle:
	.db "The Bandit Castle..",0
BanditsText4:
	.db "The Bandit King!",0
BanditsText5:
	.db "He has",0
force:
	.db "Odd forces push you back",0
NewGame:
	.db "3. New Game",0
SaveGame:
	.db "1. Game one     2. Game two",0

Saved:
	.db "Saved",0
Loaded:
	.db "Loaded",0

YouDodge:
	.db "You dodge",0

ThankYou:
	.db "Thank ye",0
WeaponShop:
	.db "Blade:",0
ArmorShop:
	.db "Armor:",0
willcost:
	.db "Cost:",0
WAset1:
	.db "1. Wood",0
WAset2:
	.db "2. Iron",0
WAset3:
	.db "3. Fire",0
WAset4:
	.db "4. Rune",0
WAset5:
	.db "5. Dragon",0
Restored:
	.db "Healed",0
Inn:
	.db "Welcome to the Inn",0 
OneNight:
	.db "One Night:",0

ItemShop:
	.db "Item",0
ItemStore:
	.db "3. Bomb",0
ItemMenu:
	.db "1. MediKit    "
	.db "2. MagiKit",0
DELtoExit:
	.db "DEL to exit",0
YouFind:
	.db "You find..",0
scroll:
	.db "a scroll. Gain XP",0
Youknowit:
	.db "Known",0
ChooseSpells:
	.db "Pick Spell",0
ChooseSkills:
	.db "Pick Skill",0
Allocate:
	.db "Allocate:",0
SlashAll:
	.db "CutAll",0
Regeneration:
	.db "Regen",0
HPMP:
	.db "HP<->MP",0
Smoke:
	.db "Smke",0
ReLife:
	.db "ReLife",0
Build:
	.db "Build",0
Xfrm:
	.db "Xfrm",0
Bribe:
	.db "Steal",0
MedKit:
	.db "HPkit",0
MagKit:
	.db "MPkit",0
ArcBolt: 
	.db "Bomb",0
EnemyAttacks:
	.db "attacks!!",0
damage:
	.db "dmg!",0
BlankText:
	.db "                    ",0
Victory:
	.db "Victory!",0
xp:
	.db "XP:",0
gold:
	.db "Gold:",0
ToNextLevel:
	.db "Next Lvl:",0
GameOVER:
	.db "Game Over",0
YourMP:
	.db "M:",0
Escape:
	.db "You Escape",0
Failed: 
	.db "Failed..",0
HPstr:
	.db "HP:",0
MPstr:
	.db "MP:",0
Strength:
	.db "ST:",0
Intelligence:
	.db "INT:",0
Speed:
	.db "DX:",0

Heal:
	.db "Heal",0
Burn:
	.db "Burn",0
Star:
	.db "Star",0
Rage:
	.db "Rage",0
Sire:
	.db "Sire",0
Rock:
	.db "Rock",0
Haste:
	.db "Haste",0
Doom:
	.db "Doom",0
needmp:
	.db "Need MP!",0
somegold:
	.db "some gold",0
Stats:
	.db "Status",0
Level:
	.db "Lvl:",0
Search:
	.db "..nothing",0
yesorno:
	.db "1. Yes      " 
	.db "2. No",0

StatusScreen:
 .db 4,1,28
 .db %01111111, %11111111, %11111111, %11111110
 .db %11000000, %00000000, %00000000, %00000011
 .db %10000011, %01000000, %10000000, %00000001
 .db %10000100, %01100110, %11010101, %10000001
 .db %10000010, %01001010, %10010101, %00000001
 .db %10000001, %01001010, %10010100, %10000001
 .db %10000110, %00100110, %01011101, %10000001
 .db %10000000, %00000000, %00000000, %00000001
 .db %10000111, %01000000, %00000000, %00000001
 .db %10000010, %01100100, %11010000, %00000001
 .db %10000010, %01001010, %10101000, %00000001
 .db %10000010, %01001100, %10101000, %00000001
 .db %10000111, %00100110, %10001000, %00000001
 .db %10000000, %00000000, %00000000, %00000001
 .db %10000011, %00000000, %00000000, %01000001
 .db %10000100, %00100011, %01010011, %01000001
 .db %10000010, %01010101, %01100100, %01100001
 .db %10000001, %01100101, %01000100, %01010001
 .db %10000110, %00110011, %01000011, %01010001
 .db %10000000, %00000000, %00000000, %00000001
 .db %10000100, %01000000, %00010000, %00000001
 .db %10000110, %11001100, %10000011, %00000001
 .db %10000101, %01010101, %01010100, %00000001
 .db %10000100, %01010100, %11010100, %00000001
 .db %10000100, %01001100, %01010011, %00000001
 .db %10000000, %00000000, %10000000, %00000001
 .db %11000000, %00000000, %00000000, %00000011
 .db %01111111, %11111111, %11111111, %11111110
