;		Dying Eyes
;
;  "tired disguised oblivion...is everything i do"
;
; "in the company of pseudo-suns...
;	happy birthday to me"
;
; "i think i've reached that point...
;      where giving up and going on..are both the same dead end to me
;   both the same old song"
;
; "goodbye my little moon...i'd rather look at Saturn"
;
; Innovated by Alex Highsmith
;
;START SIZE: 22271
;END SIZE: ~18000
;
;NOW = 21863 bytes
;
	.nolist		
	#include "ion.inc"
;	#include "keys.inc"
	#include "ti83plus.inc"
	.list	
#define TI83P
#define en

#ifdef en
;in the stats display
GOLD_STATS_COORD=$360F
WEAPON_COORD=$120C
ARMOR_COORD=$1809
GOLD_COORD=$1215
#else
GOLD_STATS_COORD=$3617
WEAPON_COORD=$120F
ARMOR_COORD=$1805
GOLD_COORD=$121D
#endif
#ifdef TI83P		
	.org progstart-2
	.db $BB,$6D

TEXT_MEM  = $8508
APD_BUF   = $86EC
OPSPACE   = $8478

#else		
	.org progstart

OPSPACE   = $8039  ;66
TEXT_MEM  = $80c9
APD_BUF   = $8265
;_htimesl = $4382
#endif		
	xor a	
	jr nc,StartG

 .db "Dying Eyes v1.0",0

GRAPH_MEM = plotsscreen
TEMPSPACE = APD_BUF     ;768

REGENCHECK = TEXT_MEM
TempLine = TEXT_MEM+1
PicCoor = TEXT_MEM+20
offset = TEXT_MEM+22
KURAICOUNTER = TEXT_MEM+24
ENEMYCOUNTER1 = TEXT_MEM+25
OK = TEXT_MEM+27
WHICHENEMY = TEXT_MEM+30
NUMBEROFENEMIES = TEXT_MEM+31
ENEMYHP1 = TEXT_MEM+32
ENEMYHP2 = TEXT_MEM+34
ENEMYHP3 = TEXT_MEM+36
ENEMYST1 = TEXT_MEM+38
ENEMYST2 = TEXT_MEM+40
ENEMYST3 = TEXT_MEM+42
ENEMYSPEED1 = TEXT_MEM+44
ENEMYSPEED2 = TEXT_MEM+46
ENEMYSPEED3 = TEXT_MEM+48
ENEMYCOUNTER2 = TEXT_MEM+49
DUMMY1 = TEXT_MEM+51
DUMMY2 = TEXT_MEM+52
ENEMYBACK1 = TEXT_MEM+55
ENEMYBACK2 = TEXT_MEM+57
ENEMYBACK3 = TEXT_MEM+59
BKX = TEXT_MEM+61
BKY = TEXT_MEM+62
Save = TEXT_MEM+63
COUNTER = TEXT_MEM+65
MAXDEFENSE = TEXT_MEM+66
BMPLOC = TEXT_MEM+68
MAPINDEX = TEXT_MEM+70
MAPSTART = TEXT_MEM+72
VIDINDEX = TEXT_MEM+74
WHEREAT = TEXT_MEM+76
STANDING = TEXT_MEM+78
OLDX = TEXT_MEM+84
OLDWHERE = TEXT_MEM+86
COUNT = TEXT_MEM+90
X = TEXT_MEM+91
Y = TEXT_MEM+92
SCENARIO = TEXT_MEM+108
ENEMYCOUNTER3 = TEXT_MEM+109
MAPVAR = TEXT_MEM+111
ENEMYDFD1 = TEXT_MEM+113
ENEMYDFD2 = TEXT_MEM+115
ENEMYDFD3 = TEXT_MEM+117	
XPGAINED = TEXT_MEM+119
BUILDUP = TEXT_MEM+121
RELIFE = TEXT_MEM+122

DEFENSEMAX = OPSPACE
STRENGTHMAX = OPSPACE+2
TOLEVEL = OPSPACE+4
HP = OPSPACE+6
MP = OPSPACE+8
XP = OPSPACE+10
GOLD = OPSPACE+12
MAXHP = OPSPACE+14
STRENGTH = OPSPACE+16
KURAILEVEL = OPSPACE+18
MAXMP = OPSPACE+20
SPEED = OPSPACE+22
INTELLIGENCE = OPSPACE+24
WEAPON = OPSPACE+26
DEFENSE = OPSPACE+28
DUNGEONCHECK = OPSPACE+30
DOUBLE = OPSPACE+32

StartG:
 res 1,(iy+13)	     ;VERY IMPORTANT, because i am using TXTSHADOW to replace TEXT_MEM
 set 7,(iy+$14)

 call RealG
 ld hl,TEXT_MEM
 ld (hl),0
 ld de,TEXT_MEM+1
 ld bc,127
 ldir

 res 7,(iy+$14)
 set 1,(iy+13)	     ;VERY IMPORTANT, because i am using TXTSHADOW to replace TEXT_MEM
 ret

RealG:
LOADGAME:
	ld hl,MAP2+814
	ld (hl),0
	call DRAWWALLS
	ld hl,NewGame	
	ld de,$0E05
	call DISPSTRING
	ld d,$07
	call DISPSTRING
	ld hl,AuthorTxt
	ld de,$3005
	call DISPSTRING
	ld de,$3704
	call DISPSTRING
	ld bc,$2325
	ld hl,Logo
	call NASRWARP		
LOADLOOP2:
	bcall(_getcsc)
    cp G_2
	jr z,LOADGAME2       
	cp G_3
	jr z,NEWGAME
    cp G_CLEAR
	RET Z
	cp G_1
             jr nz,LOADLOOP2
LOADGAME1:
	ld hl,SaveSSI
	ld (DUMMY1),hl
        	LD hl,SAVERAM
	ld de,scenario1
	jr LOADIT
LOADGAME2:	
	ld hl,SaveSSI2
	ld (DUMMY1),hl
        	LD hl,SAVERAM2
	ld de,scenario2
LOADIT:
	push de
	LD DE,TOLEVEL
        	LD BC,28
        	LDIR
	ld hl,Loaded
	ld de,$2F22
	call DISPSTRING

	ld hl,(DUMMY1)
	ld de,WhichSpells
 	ld bc,24
 	ldir

	pop de
	ex de,hl
	ld a,(hl)
	ld (SCENARIO),a	
	call WaitKey
	call ERASESCREEN
	ld a,(DUNGEONCHECK)
	cp 8
	jp z,TOTOWN2
	jp FROMLOAD
NEWGAME:
	ld de,Chests
	ld hl,MAP2
	call CHESTINIT
	ld hl,MAP3
	ld de,Chests2
	call CHESTINIT

INITSLOTS:
	 ld hl,ZeroSet
	 ld de,WhichSpells
 	 ld bc,24
 	 ldir
	xor a
	ld (DOUBLE),a
	ld (COUNTER),a
	ld (SCENARIO),a
	ld (LEADERCHECK),a	
	inc a
	ld (KURAILEVEL),a
	ld (DUNGEONCHECK),a

	ld hl,0
	ld (TOLEVEL),hl
	ld (MAXHP),hl
	ld (MAXMP),hl
	ld (STRENGTH),hl
	ld (INTELLIGENCE),hl
	ld (SPEED),hl
	ld (XP),hl
	ld (GOLD),hl

	ld hl,5
	ld (DEFENSE),hl
	ld (WEAPON),hl
	call DRAWWALLS
	ld hl,172
	call LEVELUP ;**
	call DRAWWALLS
	call INVERTSCREEN
	set 3,(IY+05)
	ld de,$1720
	ld hl,Awakened
	call DISPSTRING3
	ld hl,KuraiText1
	call KURAITALKS
START:
        	LD DE,29
        	LD HL,$001D
	jp MAP2PLACER
UPDATEMOVE:			; SET (X) TO NEW X,Y COORD
	LD (WHEREAT),HL		; SET HL TO POINT TO TOP-LEFT CORNER 
DRAWMAP:			; (X) AND (WHEREAT) SHOULD STILL BE UNCHANGED
	call REFRESHMAP
	LD A,(HL)
        	LD HL,(X)
	LD (OLDX),HL
	LD HL,(WHEREAT)
	LD (OLDWHERE),HL
	ld a,(COUNTER)
	inc a
	ld (COUNTER),a
	cp 20
	jp z,INITBATTLE
KEYLOOP:
    bcall(_getcsc)
	CP G_2nd
    jp z,STATUSSCREEN
	CP G_LEFT
	JR Z,LEFT
    CP G_RIGHT
	JR Z,RIGHT
    CP G_UP
	jr z,UP
    CP G_CLEAR
    RET Z
    CP G_DOWN
	JR NZ,KEYLOOP
DOWN:
	LD HL,(STANDING)
    LD DE,36
	ADD HL,DE
	LD A,(HL)
	cp 3
	jr nc,SOMETHING
	LD A,(Y)
	INC A
	LD (Y),A
	LD HL,(WHEREAT)
        	LD DE,36
	ADD HL,DE
	jr UPDATEMOVE

LEFT:
	LD HL,(STANDING)
	dec hl
	LD A,(HL)
	cp 3
	jr nc,SOMETHING
	LD A,(X)
	DEC A
	LD (X),A
	LD HL,(WHEREAT)
	dec hl
	jr UPDATEMOVE

RIGHT:
	LD HL,(STANDING)
	inc hl
	LD A,(HL)
	cp 3
	jr nc,SOMETHING
	LD A,(X)
	INC A
	LD (X),A
	LD HL,(WHEREAT)
	inc hl
	jp UPDATEMOVE

UP:
	LD HL,(STANDING)
        	LD DE,$FFDC
	ADD HL,DE
	LD A,(HL)
	CP 3
	JR NC,SOMETHING
        	LD A,(Y)
	DEC A
	LD (Y),A
	LD HL,(WHEREAT)
        	LD DE,$FFDC
	ADD HL,DE
	jp UPDATEMOVE
SOMETHING:
	ld a,(hl)
        	CP 7
        	jp z,KEYLOOP
	CP 6
	jp z,TOWN
	CP 5
	jp z,CASTLEORMAN
	cp 4
	jp z,CAVEORELSE
STAIRS:
	ld bc,(X)
	ld a,(DUNGEONCHECK)
	or a
	jp z,TOWER
	cp 4
	jp z,FAITHCAVE
	cp 7
	jp z,BANDITSTAIRS
	cp -1
	jp z,LASTSTAIRS
	cp -2
	jp z,FAITHCAVE2 ; you get the faith armor
	cp 8
	jp nc,INTOWN
	ld a,b
	cp $0C
	jr z,STAIRSOUT1
	cp $03
	jr z,STAIRS2
	or a
	jr z,STAIRSOUT2
	cp $04
	jr z,STAIRSD1
	cp $0D
	jr z,STAIRSD2
	cp $0F
	jr z,STAIRSD3
	cp $07
	jr z,STAIRSD5
	cp $0B
	jr z,STAIRSD6
	cp $08
	jr z,STAIRSD7
STAIRS1:
	ld bc,$0316  ;; Y,X
	jr CONTINUEST
STAIRS2:
	ld a,(SCENARIO)
	or a
	call z,TALK1
	ld bc,$0206  ;; Y,X
	jr CONTINUEST
STAIRSOUT1:
	ld a,(DUNGEONCHECK)
	cp 3
	jr z,STAIRSD4
	ld a,(SCENARIO)
	cp 1
	jp z,LEADER1
        	LD HL,$0C18
        	LD DE,456          
JUMPTOMAP:
	jp RETURNTOMAP2
STAIRSOUT2:
        	LD HL,$0812
        	LD DE,306             
	jr JUMPTOMAP
STAIRSD1:
	ld bc,$0D16
	jr CONTINUEST
STAIRSD2:
	ld bc,$0412
	jr CONTINUEST
STAIRSD3:
	ld bc,$0C0C
	jr CONTINUEST
STAIRSD4:
	ld bc,$0F17
	jr CONTINUEST
STAIRSD5:
	ld bc,$0B11  
	jr CONTINUEST
STAIRSD7:
	ld a,(SCENARIO)
	cp 4
	jr c,LOCKED
	jp z,LEADER3
STAIRSOUT3: ;; goes to other side
        	LD HL,$0810
        	LD DE,304
	jp RETURNTOMAP3
STAIRSD6:
	ld bc,$070C 
CONTINUEST:
	LD (X),bc
	push bc
	ld hl,0
LOOPOF36:
	ld de,36
	add hl,de	
	djnz LOOPOF36
	pop bc
	ld b,0
	add hl,bc		
	ex de,hl	
FROMPLACE:
	push de
	LD HL,BITMAPS2
	LD (BMPLOC),HL
	LD HL,(MAPVAR)
	LD (MAPSTART),HL
	pop de
	ADD HL,DE
	jp UPDATEMOVE	
LOCKED:
	call DRAWWALLS
	ld de,$1C1E
	ld hl,Locked
	call DISPSTRING	
	jp RETFROMSTATS

FAITHCAVE:
        	ld a,b
	cp $0E
	jr z,STAIRSOUT4
	cp $04
	jr z,STAIRSE2
	cp $02
	jr z,STAIRSE3
	cp $0B
	jr z,STAIRSE4
	cp $09
	jr z,STAIRSE5
	cp $03
	jr z,STAIRSE6
	cp $05
	jr z,STAIRSE7
	cp $13
	jr z,STAIRSE9
	cp $14
	jr z,STAIRSE10
	cp $01
	jr z,STAIRSE11
	cp $0C
	jr z,STAIRSE12
	or a
	jr z,STAIRSE13
	;; stairs 8 have the same coord as stairs1, so it is within staire1:
STAIRSE1:
	LD bc,$0400
	jr CONTINUEST
STAIRSE2:
	ld a,c
	cp $1F
	jr z,STAIRSE8
	LD bc,$0700
	jr CONTINUEST
STAIRSE3:
	LD bc,$0B08
CONTINUEST2: ;;to save bytes
	jr CONTINUEST
STAIRSE4:
	LD bc,$0200
	jr CONTINUEST2
STAIRSE5:
	LD bc,$0307
	jr CONTINUEST2
STAIRSE6:
	LD bc,$0907
	jr CONTINUEST2
STAIRSE7:
	LD bc,$041F
	jr CONTINUEST2
STAIRSE8:
	ld bc,$0506
	jr CONTINUEST2
STAIRSE9:
	ld bc,$0117
	jr CONTINUEST2
STAIRSE10:
	ld bc,$0C04
	jr CONTINUEST2
STAIRSE11:
	ld bc,$131D
	jr CONTINUEST2
STAIRSE12:
	ld bc,$141D
	jr CONTINUEST2
STAIRSE13:
	ld a,(SCENARIO)
	cp 6
	jp z,LEADER4
STAIRSOUT4: ;; leads out of faith cave
	LD HL,$0202
        	LD DE,74
RETURNTOMAP3C: 	;;to save bytes
	jp RETURNTOMAP3

BANDITSTAIRS:
	ld a,b
	cp $0F
	jr z,STAIRSF1
	cp $08
	jr z,STAIRSF2
	cp $13
	jr z,STAIRSOUT5
	cp $06
	jr z,STAIRSF5
	cp $0D
	jr z,STAIRSF6
	cp $0A
	jr z,STAIRSF7	
	cp $01
	jr z,STAIRSOUT6
STAIRSF4:
	ld a,c
	cp $1F
	jr z,STAIRSF8
	ld bc,$0F16
	jr CONTINUEST2
STAIRSF1:
	ld a,c
	cp $16
	jr z,STAIRSF3
	ld bc,$0813
	jr CONTINUEST2
STAIRSF2:
	ld bc,$0F09
	jr CONTINUEST2
STAIRSF6:
	ld bc,$0616
	jr CONTINUEST2
STAIRSF5:
	ld bc,$0D0F
CONTINUEST3:;; to save bytes
	jr STAIRSF5-2			;jump to another jr, really want to go to continuest2
STAIRSF8:
	ld bc,$0A15
	jr CONTINUEST3
STAIRSF3:
	ld bc,$0012
	LD (X),bc
	ld de,18
FROMPLACE2: ;; to save bytes
	jp FROMPLACE

STAIRSOUT6:
	ld a,(SCENARIO)
	cp 8
	jp z,LEADER5
STAIRSOUT5: ;; leads out of BANDITCASTLE
	LD HL,$0BFF
        	LD DE,395
	jr RETURNTOMAP3C
STAIRSF7:
	ld bc,$FF1F
	LD (X),bc
	ld de,-5
	jr FROMPLACE2	

LASTSTAIRS:
	ld a,b
	cp $12
	jp z,STAIRSL15
	cp $04
	jp z,STAIRSL16
	cp $09
	jr z,STAIRSL1
	cp $0E
	jr z,STAIRSL2
	cp $13
	jr z,STAIRSL3
	cp $06
	jr z,STAIRSL4
	cp $01
	jr z,STAIRSL5
	cp $05
	jp z,STAIRSL6
	cp $07
	jp z,STAIRSL7
	cp $08
	jp z,STAIRSL8
	cp $14
	jp z,STAIRSL9
	cp $03
	jp z,STAIRSL10
	cp $0F
	jp z,STAIRSL11
	cp $0D
	jp z,STAIRSL12
	cp $0C
	jp z,STAIRSL13
	cp $02
	jp z,STAIRSL14
STAIRSOUT7:
        	LD HL,$0418
        	LD DE,168             
	jp JUMPTOMAP	
STAIRSL1:
	ld a,c
	cp $01
	jr z,STAIRSL1LEFT
	cp $11
	jr z,STAIRSL1RIGHT
	cp $1E
	jr z,STAIRSL1RIGHTER
	jr STAIRSOUT7
STAIRSL1RIGHT:
	ld bc,$091E
	jr CONTINUEST4
STAIRSL1LEFT:
	ld bc,$0E04
	jr CONTINUEST4
STAIRSL1RIGHTER:
	ld bc,$0911
	jr CONTINUEST4
STAIRSL2:
	ld bc,$0901
	jr CONTINUEST4
STAIRSL3:
	ld a,c
	cp $09
	jr z,STAIRSL3DOWN
	cp $15
	jr z,STAIRSL3SWITCH
	ld bc,$0605
CONTINUEST4:
	jp CONTINUEST
STAIRSL3DOWN:
	ld bc,$0117
	jr CONTINUEST4
STAIRSL4:
	ld bc,$1300
	jr CONTINUEST4
STAIRSL5:
	ld a,c
	cp $17
	jr z,STAIRSL5RIGHT
	cp $16
	jr z,STAIRSL5LEFT
	cp $09
	jr z,STAIRS5UP
	jp LEADER7 ;; last battle
STAIRS5UP:
	ld bc,$0717
	jr CONTINUEST4
STAIRSL5LEFT:	
	ld bc,$050D
	jr CONTINUEST4
STAIRSL5RIGHT:
	ld bc,$1309
	jr CONTINUEST4
STAIRSL6:
	ld bc,$0116
	jr CONTINUEST4
STAIRSL7:
	ld bc,$0109
	jr CONTINUEST4
STAIRSL8:
	ld bc,$140E
	jr CONTINUEST4
STAIRSL9:
	ld bc,$0816
	jr CONTINUEST4
STAIRSL10:
	ld bc,$0F1F
	jr CONTINUEST4
STAIRSL11:
	ld bc,$031D
	jr CONTINUEST4
STAIRSL12:
	ld bc,$0C17
	jr CONTINUEST4
STAIRSL13:
	ld bc,$0D1F
	jr CONTINUEST4
STAIRSL14:
	ld bc,$1315
	jr CONTINUEST4
STAIRSL3SWITCH:
	ld bc,$021B
	jr CONTINUEST4
STAIRSL15:
	ld bc,$0411
	jr CONTINUEST4
STAIRSL16:
	ld bc,$1211
	jr CONTINUEST4
	

FAITHCAVE2: ;; where you get the faith armor
        	LD HL,$0020
        	LD DE,32
	jp JUMPTOMAP	

REFRESHMAP:

	LD HL,(WHEREAT)
	LD (MAPINDEX),HL
      LD DE,GRAPH_MEM+1
	LD (VIDINDEX),DE
	LD B,5
TOP5LOOP:
	PUSH BC
	LD HL,(MAPINDEX)		; PUT TYPE IN A, MOVE TO NEXT MAP BYTE
	LD A,(HL)
	INC HL
	LD (MAPINDEX),HL
	SLA A				; 2
	SLA A				; 4
	SLA A				; 8
	SLA A				; 16
	SLA A				; 32
	LD E,A
	LD D,0
	LD HL,(BMPLOC)
	ADD HL,DE
	LD DE,16
	ADD HL,DE
	LD DE,(VIDINDEX)
	LD BC,$0802
      call SMALLBMP
	LD HL,(VIDINDEX)
	INC HL
	INC HL
	LD (VIDINDEX),HL
	POP BC
	DJNZ TOP5LOOP
	LD HL,(MAPINDEX)
      LD DE,31
	ADD HL,DE
	LD (MAPINDEX),HL

      LD DE,GRAPH_MEM+($8919-$88B8)
	LD (VIDINDEX),DE
	LD B,3
MID5:
	PUSH BC
	LD B,5
MID5LOOP:
	PUSH BC
	LD HL,(MAPINDEX)		; PUT TYPE IN A, MOVE TO NEXT MAP BYTE
	LD A,(HL)
	INC HL
	LD (MAPINDEX),HL
	SLA A				; 2
	SLA A				; 4
	SLA A				; 8
	SLA A				; 16
	SLA A				; 32
	LD E,A
	LD D,0
	LD HL,(BMPLOC)
	ADD HL,DE
	LD DE,(VIDINDEX)
	LD BC,$1002
        	call SMALLBMP

	LD HL,(VIDINDEX)
	INC HL
	INC HL
	LD (VIDINDEX),HL
	POP BC
	DJNZ MID5LOOP

	LD HL,(VIDINDEX)
        	LD DE,182
	ADD HL,DE
	LD (VIDINDEX),HL
	LD HL,(MAPINDEX)
      LD DE,31
	ADD HL,DE
	LD (MAPINDEX),HL
	POP BC
	DJNZ MID5


Bottom11:
     LD DE,GRAPH_MEM+(($8919-$88B8)+(12*16*3))
	LD (VIDINDEX),DE
	LD B,5
Bot5LOOP:
	PUSH BC
	LD HL,(MAPINDEX)		; PUT TYPE IN A, MOVE TO NEXT MAP BYTE
	LD A,(HL)
	INC HL
	LD (MAPINDEX),HL
	SLA A				; 2
	SLA A				; 4
	SLA A				; 8
	SLA A				; 16
	SLA A				; 32
	LD E,A
	LD D,0
	LD HL,(BMPLOC)
	ADD HL,DE
	LD DE,(VIDINDEX)
	LD BC,$0802
      call SMALLBMP

	LD HL,(VIDINDEX)
	INC HL
	INC HL
	LD (VIDINDEX),HL
	POP BC
	DJNZ Bot5LOOP


	LD HL,PLAYER
        	LD DE,GRAPH_MEM+($89DD-$88B8)
	LD BC,$1002
        	call SMALLBMP
	call FastCopy
	LD HL,(WHEREAT)
        	LD DE,74
	ADD HL,DE
	LD (STANDING),HL
	ret

SMALLBMP:                     ; HL=SRC
        	PUSH AF
	XOR A			; DE=DEST
	LD (COUNT),A		; B=LENGTH
VERTLOOP:			; C=WIDTH
	PUSH BC
	LD B,0
	LDIR
	POP BC
	LD A,C
	NEG
	AND $F
        	SUB 4
VERTINCDELOOP:
	DEC A
	INC DE
	OR A
	JR NZ,VERTINCDELOOP
	LD A,(COUNT)
	INC A
	LD (COUNT),A
	CP B
	JR NZ,VERTLOOP
        	POP AF
	ret

;---------Battle Engine------------;

INITBATTLE:
	ld a,(DUNGEONCHECK)
	cp -1
	jr z,RESTARTBATTLE
	cp 8
	jp nc,KEYLOOP
RESTARTBATTLE:
	ld b,3
INITLOOP:
	push bc
	call INVERTSCREEN
	call DELAY2
	pop bc
	djnz INITLOOP
  ld b,2
  call RANDN
  inc a         ; between 1-3
  ld (NUMBEROFENEMIES),a
	push af	
	call DRAWWALLS            	
FROMLEADER:
	ld hl,0
	ld (ENEMYSPEED2),hl
	ld (ENEMYSPEED3),hl
	ld (XPGAINED),hl
	ld a,0
LEADERCHECK = $-1 
	or a
	jr nz,ITSALEADER
;-----------------------------------skip this if it's a leader;
FROMRIGGED2:
	call PUTENEMY
  	pop af
	ld (NUMBEROFENEMIES),a
;-------------------------------------------------------------;
ITSALEADER:
	ld hl,(STRENGTH)
	ld (STRENGTHMAX),hl
	ld hl,(DEFENSE)
	ld (DEFENSEMAX),hl
	ld hl,(SPEED)
	ld de,200
	and a
	sbc hl,de
	jr c,NODOUBLE
DOUBLESPEED:
	ld a,66
	ld (DOUBLE),a
NODOUBLE:
	xor a
	ld (RELIFE),a
	ld (BUILDUP),a
	ld (REGENCHECK),a
	ld (COUNTER),a
	ld (OK),a
	ld (ENEMYCOUNTER1),a
	ld (ENEMYCOUNTER2),a
	ld (ENEMYCOUNTER3),a
	ld a,66
	ld (KURAICOUNTER),a
	call KURAI
	call BATTLEMENU
	jr SPEEDLOOP
IRIGHT:
	ld a,(BKX)
	cp 84
	jr z,SPEEDLOOP
	ld hl,ICONRING
	call drawit
	ld a,(BKX)
	add a,9
	ld (BKX),a
	ld hl,ICONRING
	call drawit
	call DELAY2
	jr SPEEDLOOP
ILEFT:
	ld a,(BKX)
	cp 66
	jr z,SPEEDLOOP
	ld hl,ICONRING
	call drawit
	ld a,(BKX)
	sub 9
	ld (BKX),a
	ld hl,ICONRING
	call drawit
	call DELAY2
SPEEDLOOP:
 	ld a,$3E
 	out (1),a
 	in a,(1)
	bit 5,a ; 2ND
 	jp z,IPRESS
	bit 3,a
 	jp z,IUP
 	bit 0,a
 	jp z,IDOWN
        	bit 1,a
        	jr z,ILEFT
        	bit 2,a
        	jr z,IRIGHT

	ld a,(OK)
	or a
	jr nz,SPEEDLOOP

	  ;; Now check ENEMYS' COUNTERS
	  ;; when ENEMYCOUNTER == ENEMYSPEED th
	  ;; attack...hence,lower value=faster attack

	ld a,(ENEMYSPEED1)
	or a
	jr z,NEXTENEMY
	ld c,a
	ld a,(ENEMYCOUNTER1)
	inc a
	ld (ENEMYCOUNTER1),a
	cp c
	jp z,ENEMYATTK1
NEXTENEMY:	
	ld a,(ENEMYSPEED2)
	or a
	jr z,NEXTENEMY2
	ld c,a
	ld a,(ENEMYCOUNTER2)
	inc a
	ld (ENEMYCOUNTER2),a
	cp c
	jp z,ENEMYATTK2
NEXTENEMY2:	
	ld a,(ENEMYSPEED3)
	or a
	jr z,COUNTERKURAI
	ld c,a
	ld a,(ENEMYCOUNTER3)
	inc a
	ld (ENEMYCOUNTER3),a
	cp c
	jp z,ENEMYATTK3
COUNTERKURAI:
	;; Draw Kurai's COUNTER
	ld a,(KURAICOUNTER)
	ld b,a
	ld c,25
	call PutPixel
	ld a,(KURAICOUNTER)
	ld b,a
	ld c,24
	call PutPixel	

	;;CHECK KURAI'S COUNTER
	ld a,(KURAICOUNTER)
	inc a
	push af
	ld a,(DOUBLE)
	cp 66
	jr z,DOUBLEIT
	pop af		
	ld (KURAICOUNTER),a
	cp 92
	jp nz, SPEEDLOOP  ;; KEEP GOING TILL 92
ITSOK2:
	ld a,1
	ld (OK),a
	jp SPEEDLOOP
DOUBLEIT:
	pop af
	inc a
	ld (KURAICOUNTER),a
	cp 92	
	jp nz, SPEEDLOOP  ;; KEEP GOING TILL 92
	jr ITSOK2
IUP:
	ld hl,ICONRING
	call drawit
	ld a,43
	ld (BKY),a
	ld hl,ICONRING
	call drawit
	call DELAY
	jp SPEEDLOOP
IDOWN:
	ld hl,ICONRING
	call drawit
	ld a,52
	ld (BKY),a
	ld hl,ICONRING
	call drawit
	call DELAY
	jp SPEEDLOOP
IPRESS:
	ld a,(OK)
	or a
	jp z,SPEEDLOOP
	ld a,(BKX)
	cp 66
	jr z,ATTACKORSPELL
	cp 75
	jr z,ITEMORSKILL
	jp FLEE
ITEMORSKILL:	
	ld a,(BKY)
	cp 43
	jp z,KURAIITEM
	jp KURAISKILL	
ATTACKORSPELL:
	ld a,(BKY)
	cp 43
	jp z,KURAIATTACK
	jp KURAIMAGIC

ENEMYATTK1:
	xor a
	ld (ENEMYCOUNTER1),a
	ld de,(ENEMYST1)
	ld bc,1
	jr ENEMYATTACK
ENEMYATTK2:
	xor a
	ld (ENEMYCOUNTER2),a
	ld de,(ENEMYST2)
	ld bc,2
	jr ENEMYATTACK
ENEMYATTK3:
	xor a
	ld (ENEMYCOUNTER3),a
	ld de,(ENEMYST3)
	ld bc,3
ENEMYATTACK:
	ld hl,(BKX)
	push hl
	push de
	ld (DUMMY1),bc
	ld hl,(DUMMY1)
	call DISPHL2	
    ld hl,EnemyAttacks
	ld de, $163F                    ;Y=2C,X=1A
	call DISPSTRING
BEGINDODGE:
	ld a,r
	srl a
	and 15   
	cp 11
	jr z,YOUDODGE	
	ld hl,(SPEED)
	ld de,40
	and a
	sbc hl,de
	jr nc,TRYAGAIN
	jr NODODGE
TRYAGAIN:
	ld a,r
	srl a
	and 15   
	cp 8
	jr nz,NODODGE
YOUDODGE:
	call ERASETEXT2
    ld hl,YouDodge
	ld de, $163B                    ;Y=2C,X=1A
	call DISPSTRING
	ld bc,$141F
	ld (DUMMY1),bc
	ld b,4
DODGELOOP:
	push bc
	ld bc,(DUMMY1)
	ld a,b
	dec a
	ld b,a		
	ld (DUMMY1),bc 
	ld hl,Kurai
        	call NASRWARP
	call DELAY
	pop bc
	djnz DODGELOOP
	pop hl
	pop de
        	ld bc,$0F29
        	ld hl,KuraiBlank
        	call NASRWARP
	call ERASETEXT
	call KURAI
	jp NEXTENEMY
NODODGE:
	call BLANK1429
	ld bc,$0F20
        	ld hl,KuraiDefend
        	call NASRWARP
	ld a,r
  	srl a
  	and 7   
	cp 5
	jr z,EDIFFATTK
	or a
	jr nz,ENOTDIFFATTK
EDIFFATTK:
	ld a,14
	ld (BKX),a
	ld b,9
EATTACKLOOP2:
	push bc
	ld a,(BKX)
	add a,2
	ld (BKX),a
	ld a,39
	ld (BKY),a
	ld hl,ERASUREX
	call drawit
	ld a,46
	ld (BKY),a
	ld hl,ERASUREX
	call drawit
	ld a,50
	ld (BKY),a
	ld hl,ERASUREX
	call drawit
	pop bc
	djnz EATTACKLOOP2
	jr DONEEATTACK
ENOTDIFFATTK:
	ld bc,$2811
	ld hl,ENEMYHIT1
	call drawit2
	call shake
	ld bc,$321B
	ld hl,ENEMYHIT1
	call drawit2
	call shake
	ld bc,$231C
	ld hl,ENEMYHIT1
	call drawit2
	call shake
DONEEATTACK:
	call ERASETEXT
    ld bc,$0F29
    ld hl,KuraiBlank
    call NASRWARP
	call KURAI
	ld de, $1E4A
    ld hl,BlankText
	call DISPSTRING
	pop de
	call RANDOM7
	ld de,(DEFENSE)
	and a
	sbc hl,de
	jr c,LESSTHANZERO	
	ex de,hl
	jr CONTINUEATTACK
LESSTHANZERO:
	ld de,1
CONTINUEATTACK:
	ld hl,(HP)	
	and a
	sbc hl, de
	ld (HP),hl
	jr c,GAMEOVER
	jr z,GAMEOVER
	ld de,$1E4C
	call DISPHL
	pop hl
	ld (BKX),hl
	jp NEXTENEMY
GAMEOVER:
	call EFFECT
	pop bc	       		
	ld a,(RELIFE)
	or a
	jr nz,BACKTOLIFE
	ld hl,GameOVER
	ld de, $1B14
	jp DISPSTRING3

BACKTOLIFE:
	xor a
	ld (RELIFE),a
	ld hl,(MAXHP)
	srl h
	rr l
	ld (HP),hl
	call HEALLOOPER
	jp MAGIDONE
FLEE:
	ld a,(LEADERCHECK)
	or a
	jp nz,SPEEDLOOP
	ld a,r
  	srl a
  	and 127 
	ld e,a
	ld d,0
	ld hl,(SPEED)
	and a
	sbc hl,de	
	jr nc,YOUFLEE
NOFLEE:
	ld hl,Failed
	ld de, $1640
	call DISPSTRING
	call ERASETEXT2	
	call REFILL
	ld a,66
	ld (KURAICOUNTER),a
	xor a
	ld (OK),a
	jp ENEMYATTK1
YOUFLEE:
	ld hl,Escape
	ld de, $1818
	call DISPSTRING
	call EFFECT
	jp FROMBATTLE
KURAIATTACK:	        
	ld de,(BKX)
	push de
	
	ld a,(NUMBEROFENEMIES)
	cp 1
	jr z,ONLYONEALIVE
	jr CURSORLOOP
ONLYONEALIVE:
	ld a,(ENEMYSPEED1)
	or a
	jr z,ONEISDEAD
	ld a,(ENEMYSPEED2)
	or a
	jr z,TWOISDEAD
	jr THREEISDEAD

ONEISDEAD:
	ld a,(ENEMYSPEED2)
	or a
	jr z,ATTACK3
	jr ATTACK2
TWOISDEAD:
	ld a,(ENEMYSPEED1)
	or a
	jr z,ATTACK3
	jr ATTACK1
CURSDESELECT:
	ld hl,CURSORATTK
	call drawit
	pop de
	ld (BKX),de
	jp SPEEDLOOP
THREEISDEAD:
	ld a,(ENEMYSPEED1)
	or a
	jr z,ATTACK2
ATTACK1:
	ld a,1
	ld (WHICHENEMY),a
	jp CURSPRESS2
ATTACK2:
	
	ld a,2
	ld (WHICHENEMY),a
	jp CURSPRESS2
ATTACK3:
	ld a,3
	ld (WHICHENEMY),a
	jp CURSPRESS2
CURSORLOOP:
	ld a,(ENEMYSPEED1)
	or a
	jr nz,CONTINUE
	ld a,2
	ld (WHICHENEMY),a
	ld bc,$172A	
	jr SOONEWASDEAD
CONTINUE:
	ld a,1
	ld (WHICHENEMY),a
	ld bc,$1714
SOONEWASDEAD:
	ld hl,CURSORATTK
	call drawit2
	call DELAY2
	call DELAY
CURSLOOP:	
	ld a,$3E
 	out (1),a
 	in a,(1)
 	bit 7,a
 	jr z,CURSDESELECT
 	bit 5,a
 	jr z,CURSPRESS
	bit 2,a
        	jr z,CURSRIGHT
	bit 1,a
        	jr nz,CURSLOOP
CURSLEFT:
	ld a,(BKX)
	cp 20
	jr z,CURSLOOP
	ld hl,CURSORATTK
	call drawit
	ld a,(BKX)
	sub 22
	ld (BKX),a
	ld hl,CURSORATTK
	call drawit
	ld a,(WHICHENEMY)
	dec a
	ld (WHICHENEMY),a
	call DELAY2
	jr z,CURSLOOP
CURSRIGHT:
	ld a,(BKX)
	cp 64
	jr z,CURSLOOP
	ld hl,CURSORATTK
	call drawit
	ld a,(BKX)
	add a,22
	ld (BKX),a
	ld hl,CURSORATTK
	call drawit
	ld a,(WHICHENEMY)
	inc a
	ld (WHICHENEMY),a
	call DELAY2
	jr z,CURSLOOP
CURSPRESS:
	ld hl,CURSORATTK
	call drawit
CURSPRESS2:
	call SWORDSWING
	ld a,r
  	srl a
  	and 7   
	cp 5
	jr z,DIFFATTK
	or a
	jr nz,NOTDIFFATTK
DIFFATTK:
	ld hl,97
	ld (DUMMY2),hl
NOTDIFFATTK:
	ld b,2
STARTSLASH:	
	push bc
	xor a
	ld (OK),a
	ld a,(LEADERCHECK)
	cp 1
	jr z,LEADERSLASH
	ld a,(WHICHENEMY)
	cp 2
	jr z,SLASHRIGHT1
	cp 3
	jr z,SLASHRIGHT2
	ld a,15
	jr DONEDUMMY
LEADERSLASH:
	ld a,$2B
	jr DONEDUMMY
SLASHRIGHT1:
	ld a,35
	jr DONEDUMMY
SLASHRIGHT2:
	ld a,55
DONEDUMMY:
	ld (DUMMY1),a
	ld a,(DUMMY2)
	cp 97
	jr z,DIFFERENTATTK
	ld a,5
	ld (DUMMY2),a
SLASHLOOP:
	ld a,(DUMMY1)
	ld b,a
	ld a,(OK)
	add a,b
	ld (BKX),a
	ld a,(DUMMY2)
	inc a
	ld (DUMMY2),a
	ld (BKY),a
	ld hl,SWORDSLASH1
	call drawit
	ld a,(OK)
	inc a
	ld (OK),a
	cp 10
	jr nz,SLASHLOOP
	pop bc
	djnz STARTSLASH
	jr DONEWITHATTK
DIFFERENTATTK:
	pop bc
	call ATTACKNUM2
	call ATTACKNUM2
DONEWITHATTK:	
	ld de,(STRENGTH)
	call RANDOM7
	ld de,(WEAPON)
	add hl,de
	ld a,(BUILDUP)
	or a
	jr z,BUILDUPOFF
BUILDUPON:
	xor a
	ld (BUILDUP),a
	add hl,hl
BUILDUPOFF:	
	ld a,(WHICHENEMY)
	cp 2
	jr z,DFDENEMY2
	cp 3
	jr z,DFDENEMY3
DFDENEMY1:
	ld de,(ENEMYDFD1)
	jr donedfd
DFDENEMY2:
	ld de,(ENEMYDFD2)
	jr donedfd
DFDENEMY3:
	ld de,(ENEMYDFD3)
donedfd:	
	and a
	sbc hl,de
	jr c,ENEMYDFDS
	jr MORETHANZERO	
ENEMYDFDS:
	ld hl,1
MORETHANZERO
	call DUMMY1DISP
    ld hl,damage
	ld de, $1647                   ;Y=2C,X=1A
	call DISPSTRING
	call ERASETEXT2
	pop de
	ld (BKX),de

	ld a,(WHICHENEMY)
	cp 2
	jr z,TOENEMY2
	cp 3
	jr z,TOENEMY3
TOENEMY1:
	ld hl,(ENEMYHP1)		;; Displays Kurai's HP
	ld de,(DUMMY1)
	and a
	sbc hl, de
	ld (ENEMYHP1),hl
	jr ATTKEND
TOENEMY2:
	ld hl,(ENEMYHP2)
	ld de,(DUMMY1)
	and a
	sbc hl, de
	ld (ENEMYHP2),hl
	jr ATTKEND
TOENEMY3:
	ld hl,(ENEMYHP3)
	ld de,(DUMMY1)
	and a
	sbc hl, de
	ld (ENEMYHP3),hl
ATTKEND:
	call c,ENEMYDEAD
	cp 199
	jp z,FROMBATTLE	
	ld a,(REGENCHECK)
	or a
	call nz,REGENEFFECT
        	jp KURAIDONE2
FROMBATTLE:
	ld a,(LEADERCHECK)
	or a
	call nz,LEADEREND
	cp -177
	jr z,LASTFADE
	call ERASESCREEN
	jp DRAWMAP
LASTFADE:
	call DRAWWALLS
	ld bc,$231D
	ld hl,Logo
	call NASRWARP	
	ld de,$1009
	ld hl,cruelplace
	call DISPSTRING	
	ld de,$1604
		;ld hl,cruelplace2
	jp DISPSTRING3
ENDSNAKE:
	call DRAWWALLS
	ld hl,YouFind
	ld de,$1820
	call DISPSTRING
	ld hl,faitharmor
	ld de,$1E20
	call DISPSTRING3
	ld hl,MAP4+533
	ld (hl),0
	ld hl,260
	ld (DEFENSE),hl
	ret
LEADEREND:
	xor a
	ld (LEADERCHECK),a
	ld a,(DUNGEONCHECK)
	cp -2
	jr z,ENDSNAKE
	ld a,(SCENARIO)
	cp 2
	jr z,ENDLEAD2
	cp 7
	jr z,ENDLEAD3
	cp 9
	jr z,ENDLEAD4
	cp 13
	jr z,YOUWINGAME
	ret
ENDLEAD2:
	call TALK3
	jp TALK4
ENDLEAD3:
	ld a,8
	ld (SCENARIO),a
	jp TALK11
ENDLEAD4:
	call DRAWWALLS
	ld de,$1C10
	ld hl,YouFind
	call DISPSTRING	
	call FAITHSWORD
	ld hl,180
	ld (WEAPON),hl
	jp TALK12
YOUWINGAME:
	call DRAWWALLS
	ld de,$1005
	ld hl,choicetext1
	call DISPSTRING	
	ld d,$16
	call DISPSTRING
	ld d,$1C
	call DISPSTRING3	

	ld de,$2205
	ld hl,choicetext4
	call DISPSTRING3	
	call DRAWWALLS
	call YESNO
	ld de,$1114
	ld hl,healher
	call DISPSTRING	
HEALHERLOOP:
	bcall(_getcsc)
             cp G_1
             jp z,BADEND
             cp G_2
	jr nz,HEALHERLOOP ; good end
	call DRAWWALLS
	ld bc,$2639	
	ld hl,endpic
	call NASRWARP	
	ld de,$0303
	ld hl,endgame1
	call DISPSTRING	
	ld d,$09
	call DISPSTRING
	ld d,$0F
	call DISPSTRING	
	ld d,$15
	call DISPSTRING	
	ld d,$1B
	call DISPSTRING
	ld d,$21
	call DISPSTRING
 	ld d,$27
	call DISPSTRING
	ld d,$2D
	call DISPSTRING3
	ld a,-177
	ret
BADEND:
	call DRAWWALLS
	ld de,$1005
	ld hl,badend1
	call DISPSTRING	
    ld d,$16
	call DISPSTRING
	ld d,$1C
	call DISPSTRING3
	ld de,$2205
	ld hl,badend4
	call DISPSTRING3	
	ld a,-177
	ret
		
KURAIMAGIC:
 ld hl,Routine6
 ld de,TEMPSPACE
 ld bc,768
 ldir
 jp TEMPSPACE

MAGIDONE:
	call DRAWWALLS
	ld a,(LEADERCHECK)
	cp 1
	jr z,DISPLEADERBACK
	ld a,(ENEMYSPEED1)
	or a
	jr z,ENEMY1DEAD
	ld hl,(ENEMYBACK1)
	ld bc,$0F39
	call NASRWARP
ENEMY1DEAD:	
	ld a,(ENEMYSPEED2)
	or a
	jr z,ENEMY2DEAD
	ld hl,(ENEMYBACK2)
	ld bc,$2539
	call NASRWARP
ENEMY2DEAD:
	ld a,(ENEMYSPEED3)
	or a
	jr z,ENEMY3DEAD
	ld hl,(ENEMYBACK3)
	ld bc,$3B39
	call NASRWARP
ENEMY3DEAD:		
	jr KURAIDONE3
DISPLEADERBACK:
	ld hl,(ENEMYBACK1) ; WHICHLEADER
	ld bc,(ENEMYBACK2) ; COORDS
	call NASRWARP
	jr KURAIDONE3
KURAIDONE2:
	call REFILL
KURAIDONE3:
	call BATTLEMENU
	call KURAI
	ld a,66
	ld (KURAICOUNTER),a
	xor a
	ld (OK),a
	jp SPEEDLOOP


KURAIITEM:	        
	;; first erases menu so the spell menu can come up...same with skill/item
	call ERASEMENU	

	ld hl,WhichItems
	ld a,(hl)
	or a	
	jr z,NEXT1I
	push hl
             ld hl,MedKit
	ld de, $1F49                    ;Y=2C,X=1A
	call DISPSTRING
	pop hl
NEXT1I:
	inc hl
	ld a,(hl)
	or a
	jr z,NEXT2I
	push hl
             ld hl,MagKit
	ld de, $2649                    ;Y=2C,X=1A
	call DISPSTRING
	pop hl
NEXT2I:
	inc hl
	ld a,(hl)
	or a
	jr z,ITEMSTART
	push hl
             ld hl,ArcBolt
	ld de, $2D49                    ;Y=2C,X=1A
	call DISPSTRING	
	pop hl
ITEMSTART:
	ld bc,$1F3F
	ld hl,CURSORMAGIC
	call drawit2
	call DELAY2

	ld hl,WhichItems
ITEMLOOP2:
	push hl
ITEMLOOP:
	ld a,$3E
 	out (1),a
 	in a,(1)
 	bit 5,a
 	jr z,ITEMPRESS
	bit 3,a
        	jr z,ITEMUP
 	bit 7,a  ; del
 	jp z,GOBACK
	bit 0,a
        	jr nz,ITEMLOOP
ITEMDOWN:
	ld a,(BKY)
	cp $34
	jr z,ITEMLOOP
	pop hl
	inc hl
	push hl
	ld hl,CURSORMAGIC
	call drawit
	ld a,(BKY)
	add a,7
	ld (BKY),a
	ld hl,CURSORMAGIC
	call drawit
	call DELAY2
	jr ITEMLOOP
ITEMUP:
	ld a,(BKY)
	cp $1F
	jr z,ITEMLOOP

	pop hl
	dec hl
	push hl
	ld hl,CURSORMAGIC
	call drawit
	ld a,(BKY)
	sub 7
	ld (BKY),a
	ld hl,CURSORMAGIC
	call drawit
	call DELAY2
	jr ITEMLOOP
ITEMPRESS:
	pop hl
	ld a,(hl)
	or a	
	jp z,ITEMLOOP2
	dec a
	ld (hl),a
	call BLANK1429
	call KURAIMAGIC1	
	ld a,(BKY)
	cp $1F
	jr z,HPkit
	cp $2D
	jr z,BOMB
MPkit:
	ld de,40
	call OVERMAXMP
	ld bc,$2B26
        	ld hl,MagicKit
        	call NASRWARP
	ld a,234
	ld (OK),a
HPkit:
	call HEALLOOPER
	ld a,(OK)
	cp 234
	jp z,MAGIDONE	
	ld de,90
	call OVERMAXHP
	jp MAGIDONE

BOMB:
	call INVERTSCREEN
	ld b,99
BOMBLOOP:
	push bc
	ld a,r
	srl a
	and 50	   
	add a,15
	ld b,a
	ld a,r
	srl a
	and 10   
	add a,50
	ld c,a
	ld hl,EnemyDead
	call NASRWARP
	pop bc 
	djnz BOMBLOOP  
	call INVERTSCREEN
	ld de,50
	jp  MAGIDAMAGE

KURAISKILL:
 ld hl,Routine5
 ld de,TEMPSPACE
 ld bc,768
 ldir
 jp TEMPSPACE

YouWin:
	call DRAWWALLS
	call KURAI
	call BATTLEMENU
	ld hl,(STRENGTHMAX)
	ld (STRENGTH),hl
	ld hl,(DEFENSEMAX)
	ld (DEFENSE),hl

	ld hl,Victory
	ld de, $0404
	call DISPSTRING
		;ld hl,xp
	ld de, $0B1D
	call DISPSTRING
		;ld hl,gold
	ld de, GOLD_COORD
	call DISPSTRING
	ld hl,(XP)
	ld de,(XPGAINED)
	add hl,de
	ld (XP),hl
	ld de,(XPGAINED)
	call RANDOM7
	ld (NUMBEROFENEMIES),hl
	ld hl,(GOLD)
	ld de,(NUMBEROFENEMIES)
	add hl,de
	ld (GOLD),hl
	ld hl,0
	ld (DUMMY1),hl
LOOPOFGOLDXP:
	ld hl,(DUMMY1)
	inc hl
	inc hl
	ld (DUMMY1),hl
	ld de,(XPGAINED)
	and a
	sbc hl,de
	jr z,DONEXP
	jr nc,DONEXP
	ld hl,(DUMMY1)
	ld de,$0B28
	call DISPHL	
	ld hl,(DUMMY1)
	ld de,$1228
	call DISPHL
	jr LOOPOFGOLDXP
DONEXP:
	ld hl,(NUMBEROFENEMIES)
	ld de,$1228
	call DISPHL
	ld hl,ToNextLevel
	ld de,$1907
	call DISPSTRING
	ld hl,(TOLEVEL)
	ld de,(XP)
	and a
	sbc hl,de
	ld de,$1928
	call DISPHL
LEVELCHECK:
	ld hl,(XP)
	ld de,(TOLEVEL)
	and a
	sbc hl,de 
	jr nc, UPLEVEL
	call WaitKey 

DONECHECK:
	ld a,199
	ret
UPLEVEL:
	ld a,(KURAILEVEL)
	inc a
	ld (KURAILEVEL),a
	ld hl,32
	call LEVELUP2
	jr DONECHECK

LEADERALLOCATE2:
	ld (ENEMYBACK1),hl
	ld (ENEMYBACK2),bc
	call NASRWARP
	call DRAWWALLS2
	ld a,1
	ld (NUMBEROFENEMIES),a
	ld (LEADERCHECK),a
 	ret

LEADER5:
	ld a,9
	ld (SCENARIO),a
	call DRAWWALLS
	ld de,$1610
	ld hl,BanditsText4
	call DISPSTRING	
		;ld de,$1C10
		;ld hl,BanditsText5
	ld d,$1C
	call DISPSTRING	
	call FAITHSWORD
	call ERASESCREEN
	ld hl,Leader5
	ld bc,$1C3C
	call LEADERALLOCATE2
	ld bc,170
	ld de,2500
	ld ix,37
	ld hl,90
	jr LEADERALLOCATE

LEADER1:
	call TALK2
	ld a,2
	ld (SCENARIO),a
LEADER1TOWN:
	ld hl,Leader1
	ld bc,$283C
	call LEADERALLOCATE2
	ld bc,25
	ld de,250
	ld ix,34
	ld hl,13
LEADERALLOCATE:
	ld (ENEMYST1),bc	
	ld (ENEMYHP1),de
	ld (ENEMYSPEED1),ix
	ld (ENEMYDFD1),hl
	jp FROMLEADER

LEADER2:
	call ERASESCREEN
	ld hl,Leader2
	ld bc,$283C
	call LEADERALLOCATE2
	ld bc,40
	ld de,400
	ld ix,55
	ld hl,40
	jr LEADERALLOCATE
LEADER3:
	call TALK5
	ld hl,Leader3
	ld bc,$093C
	call LEADERALLOCATE2
	ld bc,60
	ld de,1000
	ld ix,45
	ld hl,20
	ld a,5
	ld (SCENARIO),a
TOLEADER:
	jr LEADERALLOCATE
LEADER6:
	call DRAWWALLS
	ld de,$1010
	ld hl,Serpent1
	call DISPSTRING	
		;ld de,$1C10
		;ld hl,faitharmor
	ld d,$1C
	call DISPSTRING	
		;ld de,$1610
		;ld hl,Serpent2
	ld d,$16
	call DISPSTRING3	
	call ERASESCREEN
	ld hl,Leader6
	ld bc,$093C
	call LEADERALLOCATE2
	ld bc,330
	ld de,10000
	ld ix,50
	ld hl,900
	jr TOLEADER
LEADER7:
	call DRAWWALLS

	ld de,$1003
	ld hl,lasttext1
	call DISPSTRING	
		;ld de,$1603
		;ld hl,lasttext2
	ld d,$16
	call DISPSTRING3
	call DRAWWALLS
		;ld de,$1005
		;ld hl,lasttext3
	ld d,$10
	call DISPSTRING3	
		;ld de,$1605
		;ld hl,lasttext4
	ld d,$16
	call DISPSTRING3	
	call DRAWWALLS
		;ld de,$1005
		;ld hl,lasttext5
	ld d,$10	
	call DISPSTRING	
		;ld de,$1605
		;ld hl,lasttext6
	ld d,$16
	call DISPSTRING
		;ld de,$1C05
		;ld hl,lasttext7
	ld d,$1C
	call DISPSTRING3
	ld b,70
FLASHSCREEN3:	
	push bc
	call INVERTSCREEN
	pop bc
	djnz FLASHSCREEN3
	ld de,$2205
	ld hl,lasttext8
	call DISPSTRING3
	call DRAWWALLS
		;ld de,$1005
		;ld hl,lasttext9
	ld d,$10	
	call DISPSTRING	
		;ld de,$1605
		;ld hl,lasttext10
	ld d,$16
	call DISPSTRING3	
	call ERASESCREEN
	ld hl,Leader7
	ld bc,$093C
	call LEADERALLOCATE2
	ld bc,360
	ld de,10000
	ld ix,30
	ld hl,200
	ld a,13 ; end of game
	ld (SCENARIO),a
	jp LEADERALLOCATE
LEADER4:
	call DRAWWALLS
	ld de,$1614
	ld hl,BanditsText1 ;; The Faith sword!!
	call DISPSTRING	
	push hl
	CALL TALK10
	call DRAWWALLS
	pop hl
		;ld de,$1610
		;ld hl,BanditsText2
	ld e,$10
	call DISPSTRING	
		;ld de,$1C10
		;ld hl,BanditsText3
	ld d,$1C
	call DISPSTRING	
	call FAITHSWORD
	call DRAWWALLS
	ld a,7
	ld (SCENARIO),a
	ld (LEADERCHECK),a
	ld a,3
	ld (NUMBEROFENEMIES),a
	push af
	jp FROMRIGGED2
RIGGEDENEMY:
	xor a
	jr FROMRIGGED
PUTENEMY:
	ld a,(SCENARIO)
	cp 7
	jr z,RIGGEDENEMY
  ld a,r
  srl a
  and 6   
  ld (WHICHENEMY),a
FROMRIGGED:
	push af
	ld a,(DUNGEONCHECK)
	cp 1 ;; CASTLE DUNGEON
	jp z,DUNGEON1
	cp 3 ;; WEST CAVE TO KIATA
	jp z,DUNGEON3
	cp 4 ;; FAITH CAVE
	jp z,DUNGEON6
	cp 7 ;; BANDIT CASTLE
	jp z,DUNGEON7
	cp -1 ;; LAST CAVE
	jp z,DUNGEON8
	cp 5 ;; OTHER SIDE OF OVERWORLD MAP
	jp z,DUNGEON5
	pop af ;; AND THIS WOULD BE THE OVERWORLD MAP, PART 1
	or a
	jr z,Enemy1
	cp 2
	jr z,Enemy2
	cp 4
	jr z,Enemy3

Enemy4:		;; bc == strength de == life
	ld bc,15	;; ix == speed
	ld de,35
	ld ix,45
	ld hl,22
	ld (DUMMY1),hl
       	ld hl,ENEMY4
	jr STARTPUT1
Enemy3:
	ld bc,20
	ld de,40
	ld ix,50
	ld hl,22
	ld (DUMMY1),hl
       	ld hl,ENEMY3
	jr STARTPUT1
Enemy2:
	ld bc,10
	ld de,40
	ld ix,30
	ld hl,10
	ld (DUMMY1),hl
       	ld hl,ENEMY2
	jr STARTPUT1
Enemy1:
	ld bc,30
	ld de,66
	ld ix,66
	ld hl,30
	ld (DUMMY1),hl
       	ld hl,ENEMY1
	jr STARTPUT1
DUNGEON1:
	ld hl,8
	ld (DUMMY1),hl
	pop af
	or a
	jr z,Enemy5
	cp 2
	jr z,Enemy6
	cp 4
	jr z,Enemy7

Enemy8:		;; bc == strength de == life
	ld bc,15	;; ix == speed
	ld de,20
	ld ix,45
       	ld hl,ENEMY8
	jr STARTPUT1
Enemy7:
	ld bc,10
	ld de,30
	ld ix,50
	ld hl,24
	ld (DUMMY1),hl
       	ld hl,ENEMY7
STARTPUT1:
	jr STARTPUT2
Enemy6:
	ld bc,08
	ld de,18
	ld ix,27
       	ld hl,ENEMY6
	jr STARTPUT2
Enemy5:
	ld bc,20
	ld de,43
	ld ix,60
       	ld hl,ENEMY5
	jr STARTPUT2
DUNGEON3:
	ld hl,32
	ld (DUMMY1),hl
	pop af
	or a
	jr z,Enemy9
	cp 2
	jr z,Enemy10
	cp 4
	jr z,Enemy11
Enemy12:		;; bc == strength de == life
		;; ix == speed
	ld bc,24
	ld de,120
	ld ix,18
	ld (DUMMY1),ix
       	ld hl,ENEMYGIMP ;; weak guy you fight in town
	jr STARTPUT
Enemy11:
	ld bc,35
	ld de,90
	ld ix,50
       	ld hl,ENEMY11
	jr STARTPUT
Enemy10:
	ld bc,40
	ld de,100
	ld ix,27
       	ld hl,ENEMY10
	jr STARTPUT
Enemy9:
	ld bc,60
	ld de,150
	ld ix,60
       	ld hl,ENEMY9
STARTPUT2:
	jr STARTPUT

DUNGEON5: ;;overhead map #2
	ld hl,45
	ld (DUMMY1),hl
	pop af
	or a
	jr z,Enemy9
	cp 2
	jr z,Enemy13
	cp 4
	jr z,Enemy14
Enemy15:
	ld bc,65
	ld de,140
	ld ix,45
       	ld hl,ENEMY15
	jr STARTPUT
Enemy14:
	ld bc,80
	ld de,150
	ld ix,50
       	ld hl,ENEMY14
	jr STARTPUT
Enemy13:
	ld bc,60
	ld de,160
	ld ix,40
       	ld hl,ENEMY13
	jr STARTPUT

STARTPUT:
	ld a,(KURAICOUNTER)
	cp 199
	jr z,PUT2
	cp 201
	jr z,PUT3
PUT1:
	ld (ENEMYST1),bc	
	ld (ENEMYHP1),de
	ld (ENEMYSPEED1),ix
	ld (ENEMYBACK1),hl
	ld bc,$0F39
	call NASRWARP
	ld hl,(DUMMY1)
	ld (ENEMYDFD1),hl
	ld a,(NUMBEROFENEMIES)
	dec a
	or a
	ret Z
	ld (NUMBEROFENEMIES),a
	ld a,199
	ld (KURAICOUNTER),a
	jr PUTENEMY2
PUT2:
	ld (ENEMYST2),bc	
	ld (ENEMYHP2),de
	ld (ENEMYSPEED2),ix
	ld (ENEMYBACK2),hl
	ld bc,$2539
	call NASRWARP
	ld hl,(DUMMY1)
	ld (ENEMYDFD2),hl
	ld a,(NUMBEROFENEMIES)
	dec a
	or a
	ret Z
	ld (NUMBEROFENEMIES),a
	ld a,201
	ld (KURAICOUNTER),a
PUTENEMY2:
	jp PUTENEMY
PUT3:
	ld (ENEMYST3),bc	
	ld (ENEMYHP3),de
	ld (ENEMYSPEED3),ix
	ld (ENEMYBACK3),hl
	ld bc,$3B39
	call NASRWARP
	ld hl,(DUMMY1)
	ld (ENEMYDFD3),hl
	ret
DUNGEON6:
	ld hl,45
	ld (DUMMY1),hl
	pop af
	or a
	jr z,Enemy18
	cp 2
	jr z,Enemy17
	cp 4
	jr z,Enemy19
Enemy16:		;; bc == strength de == life
		;; ix == speed
	ld bc,90
	ld de,80
	ld ix,18
	ld (DUMMY1),ix
       	ld hl,ENEMY16
STARTPUT3:
	jp STARTPUT
Enemy18:
	ld bc,110
	ld de,210
	ld ix,50
       	ld hl,ENEMY18
	jr STARTPUT3
Enemy17:
	ld bc,70
	ld de,130
	ld ix,34
       	ld hl,ENEMY17
	jr STARTPUT3
Enemy19:
	ld bc,78
	ld de,150
	ld ix,30
       	ld hl,ENEMY19
	jr STARTPUT3

DUNGEON7:
	ld hl,60
	ld (DUMMY1),hl
	pop af
	or a
	jr z,Enemy20
	cp 2
	jr z,Enemy21
	cp 4
	jr z,Enemy18
Enemy22:
	ld bc,100
	ld de,300
	ld ix,50
       	ld hl,ENEMY22
	jr STARTPUT3
Enemy21:
	ld bc,140
	ld de,150
	ld ix,34
       	ld hl,ENEMY21
	jr STARTPUT3
Enemy20:
	ld hl,190
	ld (DUMMY1),hl

	ld bc,100
	ld de,200
	ld ix,40
       	ld hl,ENEMY20
	jr STARTPUT3
DUNGEON8:
	ld hl,100
	ld (DUMMY1),hl
	pop af
	or a
	jr z,Enemy21
	cp 2
	jr z,Enemy23
	cp 4
	jr z,Enemy24
Enemy25:
	ld bc,300
	ld de,300
	ld ix,50
       	ld hl,ENEMY25
STARTPUT4:
	jp STARTPUT
Enemy23:
	ld bc,440
	ld de,200
	ld ix,34
       	ld hl,ENEMY23
	jr STARTPUT4
Enemy24:
	ld bc,200
	ld de,200
	ld ix,40
       	ld hl,ENEMY24
	jr STARTPUT4
GIMPMAN:
	call DRAWWALLS        	
	call Enemy12
	ld a,1
	ld (NUMBEROFENEMIES),a
	dec a
	ld (ENEMYSPEED2),a
	ld (ENEMYSPEED3),a
	jp ITSALEADER

NASRWARP:      
	ld de,$0000
        push    bc
        ex      de,hl
        call FIND_PIXEL        
        ld      bc,GRAPH_MEM
        add     hl,bc
        ld      (PicCoor),hl
        ex      de,hl
        ld      b,$ff
Patch:
        rla
        inc     b
        jr      nc,Patch
        ld      a,(hl)
        ld      c,a      
        inc     hl
        ld      a,(hl)
        ld      (offset),a
        ld      a,b   
        ld      e,c
        cp      (hl)
        jr      c,DS1
        inc     e
DS1:
        inc     hl
        ld      a,(hl)
        ld      d,a
        inc     hl
YLoop:            
        push    bc 
        push    de
        ld      b,8
        ex      de,hl
        ld      hl,TempLine
InitTempLineLoop:
        ld      (hl),$ff
        inc     hl
        djnz    InitTempLineLoop
        ex      de,hl
NASRDraw:
        ld      b,0
        ld      de,TempLine
       ldir       

NASRDrawFin:
       pop     de
        pop     bc

        push    hl
        push    bc         
        push    de
        ld      d,b
        ld      a,b
        or a
        jr      z,skipshift
SpriLoop1:
        ld      a,b    
        ld      HL,TempLine
        ld      b,e
        scf
SpriLoop2:        
        rr      (HL)
        inc     HL
        djnz    SpriLoop2
        ld      b,a
        djnz    SpriLoop1        
        ld      b,d
skipshift:
        ld      a,$ff           ;fill accumulator
        inc     b
mask1:
        srl     a               ;make the mask
        djnz    mask1           ;something like
        scf                     ;00001111 ->
        rl      a                        
        ld      hl,(PicCoor)    ;implement the mask
        or      (hl)            ;this preserve the 0 bits
        ld      hl,TempLine   ;become xxxx1111
        and     (hl)            ;when anded, become
        ld      (hl),a          ;xxxxyyyy
        ld      b,d             ;retrieve b
        ld      a,(offset)
        dec     a
        sub     b
        jr      nc,skip
        sub     248
skip:
        ld      b,a
        inc     b
        ld      a,$ff
mask2:               
        sla     a
        djnz    mask2
        scf
        rr      a
        dec     e
        ld      hl,(PicCoor)
        ld      d,0
        add     hl,de
        or      (hl)
        ld      hl,TempLine
        add     hl,de
        and     (hl)
        ld      (hl),a
        inc     e
        ld      c,e
        ld      hl,(PicCoor)
        ld      de,12   
        push    hl
        add     hl,de
        ld      (PicCoor),hl
        pop     hl
        ld      de,TempLine
        ex      de,hl
        ld      b,0
        ldir      
        pop     de
        pop     bc
        pop     hl
        dec     d
        jp nz,YLoop
        pop     bc
        jp FastCopy

drawit2:
	ld (BKX),bc
drawit:
    push hl
	push hl
    pop ix
    ld  a,(BKY) 	 ; Load the y-coordinate in c
    ld  l,a
	ld a,(BKX)	 ; Load the x-coordinate in b
    ld b,8
JOESPRITE:
    ld	e,l
	ld	h,$00
	ld	d,h
	add	hl,de
	add	hl,de
	add	hl,hl
	add	hl,hl
	ld	e,a
	and	$07
	ld	c,a
	srl	e
	srl	e
	srl	e
	add	hl,de
	ld	de,GRAPH_MEM
	add	hl,de
sl1:
	ld	d,(ix)
	ld	e,$00
	ld	a,c
	or	a
	jr	z,sl3
sl2:
	srl	d
	rr	e
	dec	a
	jr	nz,sl2
sl3:
	ld	a,(hl)
	xor	d
	ld	(hl),a
	inc	hl
	ld	a,(hl)
	xor	e
	ld	(hl),a
	ld	de,$0B
	add	hl,de
	inc	ix
	djnz	sl1
	call FastCopy
	pop hl
	ret


SPELLCAST:
	ld (MP),hl
	call BLANK1429
	call KURAIMAGIC1
	ld bc,$1025
        	ld hl,KuraiOrb1
        	call NASRWARP
        	ld bc,$2225
        	ld hl,KuraiOrb1
        	call NASRWARP
        	call DELAY
	ld bc,$1025
        	ld hl,KuraiOrb2
        	call NASRWARP
	ld bc,$2225
        	ld hl,KuraiOrb2
        	call NASRWARP
	call DELAY
	ld bc,$1025
        	ld hl,KuraiOrb3
        	call NASRWARP
	ld bc,$2225
        	ld hl,KuraiOrb3
        	call NASRWARP
	call DELAY
	ld bc,$1025
        	ld hl,KuraiOrb4
        	call NASRWARP
	ld bc,$2225
        	ld hl,KuraiOrb4
        	call NASRWARP
	call DELAY
	
	ld b,3
FIRELOOP:
	push bc
	ld bc,$111D
        	ld hl,KuraiMagic2
        	call NASRWARP
	call DELAY
	call KURAIMAGIC1
	call DELAY
	pop bc
	djnz FIRELOOP
	ret

;;DM_HL_DECI
DISPHL2:	
	ld de,$163A
DISPHL:
DM_HL_DECI:
  ld   (pencol),de          ;Since DE is a word, it overlaps into 8216
  ld b,48                  ; 48 -> b
  ld c,4                   ; 4 -> c
  ld a,h                   ; h -> a
  or l                     ; If a=0 and l=0, then return zero
  jr z,DM_HL_3             ; If zero, then jump to DM_HL_3
  ld c,-1                  ; -1 -> c
  ld b,46                  ; 46 -> b
  jr DM_HL_NEXT2           ; Jump to DM_HL_NEXT2
DM_HL_INIT:
  bcall(_cphlde)              ; cp hl,de
  jr c,DM_HL_NEXT2         ; If carry, then jump to DM_HL_NEXT2
  dec c                    ; c = c - 1
DM_HL_NEXT:
  ld b,47                  ; 47 -> b
DM_HL_NEXT2:
  inc c                    ; c = c + 1
  ld a,c                   ; c -> a
  or a                     ; If a=0, then return zero
  ld de,10000              ; 10000 -> de
  jr z,DM_HL_2a            ; If zero, then jump to DM_HL_2a
  cp 1                     ; If a=1, then return zero
  ld de,1000               ; 1000 -> de
  jr z,DM_HL_2a            ; If zero, then jump to DM_HL_2a
  cp 2                     ; If a=2, then return zero
  ld de,100                ; 100 -> de
  jr z,DM_HL_2a            ; If zero, then jump to DM_HL_2a
  cp 3                     ; If a=3, then return zero
  ld de,10                 ; 10 -> de
  jr z,DM_HL_2a            ; If zero, then jump to DM_HL_2a
  ld de,1                  ; 1 -> de
DM_HL_2a:
  ld a,b                   ; b -> a
  cp 46                    ; If a=46, then return zero
  jr z,DM_HL_INIT          ; If zero, then jump to DM_HL_INIT
DM_HL_2b:
  inc b                    ; b = b + 1
  bcall(_cphlde)           ; cp hl,de
  jr c,DM_HL_3             ; If carry, then jump to DM_HL_3
  or a                     ; Reset carry flag
  sbc hl,de                ; hl = hl - de
  jr DM_HL_2b              ; Jump to DM_HL_2b
DM_HL_3:
  ld a,b                   ; b -> a
  bcall(_vputmap)    ; Display the character
  ld a,c                   ; c -> a
  cp 4                     ; If a=4, then return zero
  jr nz,DM_HL_NEXT         ; If not zero, then jump to DM_HL_NEXT
  jp FastCopy

DISPSTRING2: ;; to optimize :)
	ld de,$163A
DISPSTRING:
	ld (pencol),de
	bcall(_vputs)     ;Display the string
	jp FastCopy 		; Copy graphbuf to LCD

ERASESCREEN:
	ld hl,GRAPH_MEM
	ld (hl),0
	ld de,GRAPH_MEM+1
 	ld bc,767
	ldir
    ret


ERASETEXT2:
	call DELAY2	
	call DELAY2
ERASETEXT:
            	ld hl,BlankText
				push hl
            	call DISPSTRING2
				pop hl
            		;LD de,$1649		
            	ld e,$49
					;ld hl,BlankText
            	jp DISPSTRING

PutPixel:		;puts the pixel at b, c
	call FIND_PIXEL
	ld de,GRAPH_MEM
	add hl, de
	or (hl)
	ld (hl),a
    jp FastCopy

;-------------------------------------------------------------------------------
;FIND_PIXEL Routine, by Patrick Davidson [From Zkart3d 82]
;-------------------------------------------------------------------------------
FIND_PIXEL_DATA:
         .db      128,64,32,16,8,4,2,1
FIND_PIXEL:
         push     bc
	   push     de
         ld       a,b
         and      7
         ld       hl,FIND_PIXEL_DATA
         ld       e,a
         ld       d,0
         add      hl,de
         ld       e,(hl)            ; E = pixel mask
         ld       a,63
         sub      c
         ld       c,a
         add      a,a
         add      a,c               ; A = 3 * Y
         ld       l,a
         ld       h,0
         add      hl,hl
         add      hl,hl             ; HL = 12 * Y
         ld       a,b
         ld       b,0
         rrca
         rrca
         rrca
         and      15
         ld       c,a
         add      hl,bc
         ld       a,e               ; A = pixel mask
         pop      de
         pop      bc
         ret

DRAWWALLS:
 push hl
	call ERASESCREEN
 POP HL
DRAWWALLS2:
 PUSH HL		
	LD HL,GRAPH_MEM
	LD DE,GRAPH_MEM+1
           	LD BC,23
           	LD (HL),$FF
           	LDIR
	LD HL,GRAPH_MEM+($8BA0-$88B8)
	LD DE,GRAPH_MEM+($8BA1-$88B8)
           	LD BC,23
           	LD (HL),$FF
           	LDIR
           	
	LD B,60
           	LD HL,GRAPH_MEM+($88D0-$88B8)
           	LD DE,11
SIDELINELOOP:
           	LD (HL),$C0
           	ADD HL,DE
           	LD (HL),3
           	INC HL
           	DJNZ SIDELINELOOP
	call FastCopy
	pop hl
    ret

shake:
	ld      a,42h
	out     (10h),a
	call DELAY2
	ld      a,40h
	out     (10h),a
	jp DELAY2
RANDOM7:
  ld a,r
  srl a
  and 7   
  ld l,a
  ld h,0
  add hl,de
  ret

DELAY:
        ld a,$4F
        ld b,$4F
dloop:
        dec a
        jr nz,dloop
        dec b
        jr nz,dloop
        ret
DELAY2:
        ld a,$9F
        ld b,$9F
        jr dloop
DELAY3:
        ld a,$FF
        ld b,$FF
        jr dloop

BATTLEMENU:
	ld bc,$3F22
        	ld hl,BattleMenu	;; Displays the Menu
        	call NASRWARP
	ld hl,(HP)		;; Displays Kurai's HP
	ld de,$1E4C
	call DISPHL
	ld bc,$2B42
	ld hl,ICONRING
	jp drawit2
ERASEMENU:
	ld bc,$4020
        	ld hl,KuraiBlank2	;; Displays the Menu
        	call NASRWARP
	call DRAWWALLS2

	ld bc,$1658
	ld hl,LINE
	jp drawit2
OVERMAXHP:
	ld hl,(HP)
	add hl,de
	ld (HP),hl
	ld de,(MAXHP)
	and a
	sbc hl,de
	ret C
	ld hl,(MAXHP)			
	ld (HP),hl
	ret

OVERMAXMP:
	ld hl,(MP)
	add hl,de
	ld (MP),hl
	ld de,(MAXMP)
	and a
	sbc hl,de
	ret C
	ld hl,(MAXMP)			
	ld (MP),hl
	ret

INVERTSCREEN:	;;inverts screen		
	ld      de,768
	ld      hl,GRAPH_MEM
invloop:
	ld      a,(hl)
	cpl
	ld      (hl),a
	inc     hl
	dec     de
	ld      a,d
	or      e
	jr      NZ,invloop
	jp      FastCopy

GOBACK:
	pop hl
	call ERASETEXT
	call BATTLEMENU
	call REFILL
	call KURAI
	ld a,1
	ld (OK),a
	jp  SPEEDLOOP

REFILL:
	ld bc,(BKX)
	push bc	
	ld bc,$2042
	ld hl,ERASURE
	call drawit2
	ld a,74
	ld (BKX),a
	ld hl,ERASURE
	call drawit
	ld a,82
	ld (BKX),a
	ld hl,ERASURE
	call drawit
	ld a,84
	ld (BKX),a
	ld hl,ERASURE2
	call drawit
	pop bc
	ld (BKX),bc
	ret

SLASH_ALL:	;; star spell and slash-all gfx
	ld b,2
STARTSTAR:	
	push bc
	xor a
	ld (OK),a
	ld a,15
	ld (DUMMY1),a
	ld a,5
	ld (DUMMY2),a
STARLOOP:
	ld a,(DUMMY1)
	ld b,a
	ld a,(OK)
	add a,b
	ld (BKX),a
	ld a,(DUMMY2)
	ld (BKY),a
	ld hl,SWORDSLASH1
	call drawit
	ld a,(DUMMY1)
	add a,20
	ld b,a
	ld a,(OK)
	add a,b
	ld (BKX),a
	ld a,(DUMMY2)
	ld (BKY),a
	ld hl,SWORDSLASH1
	call drawit
	ld a,(DUMMY1)
	add a,40
	ld b,a
	ld a,(OK)
	add a,b
	ld (BKX),a
	ld a,(DUMMY2)
	ld (BKY),a
	inc a
	ld (DUMMY2),a
	ld hl,SWORDSLASH1
	call drawit
	ld a,(OK)
	inc a
	ld (OK),a
	cp 10
	jr nz,STARLOOP
	pop bc
	djnz STARTSTAR
	ret
SWORDSWING:
	call BLANK1429
       	ld bc,$0B29
        	ld hl,Kurai4
        	call NASRWARP
        	call DELAY
        	ld bc,$0B29
        	ld hl,KuraiBlank
        	call NASRWARP
        	ld bc,$0B1C
        	ld hl,Kurai3
        	call NASRWARP
        	call DELAY
        	ld bc,$0B29
        	ld hl,KuraiBlank
        	call NASRWARP
        	ld bc,$031A
        	ld hl,Kurai5
        	call NASRWARP
	call DELAY
        	ld bc,$0329
        	ld hl,KuraiBlank
        	call NASRWARP
	jp KURAI	

KURAI:
        	ld bc,$141F
        	ld hl,Kurai
        	jp NASRWARP
DISPMP:

	ld hl,YourMP
	ld de, $164A                   ;Y=2C,X=1A
	call DISPSTRING
	ld hl,(MP)
		;ld de,$1652
	ld e,$52
	jp DISPHL
EFFECT:
	LD B,1
EFFECT1:
    LD HL,GRAPH_MEM+($88D0-$88B8)
    LD DE,6
	push bc
SIDELINELOOP2:
    LD A,$C0
    LD (HL),0
    ADD HL,DE
    LD (HL),0
    INC HL
    DJNZ SIDELINELOOP2
	call FastCopy
	pop bc
	inc b
	ld a,b
	cp 105
	jr nz,EFFECT1
	ret

LEVELUP2:
	push hl
	call DRAWWALLS
	ld hl,LevelUp
	ld bc,$3E3B
	call NASRWARP	
	pop hl
LEVELUP:
 push hl
 ld hl,Routine1
 ld de,TEMPSPACE
 ld bc,768
 ldir
 pop hl
 jp TEMPSPACE
	

ITEMHEALS:
	ld hl,HPstr
	ld de,$1217
	call DISPSTRING
	ld de,$1815
	call DISPSTRING
ITEMHEALS2:
	ld hl,(HP)	
	ld de,$1222
	call DISPHL
	ld hl,(MP)	
	ld de,$1822
	jp DISPHL

INTOWN:
	ld a,(DUNGEONCHECK)
	cp 9
	jr z,TOWN1
	cp 10
	jr z,CAVEOLDMAN
TOWN2:
        	LD HL,$0808
        	LD DE,296
	jr RETURNTOMAP3
CAVEOLDMAN:
        	LD HL,$1002
        	LD DE,578
	jr RETURNTOMAP3
TOWN1:
        	LD HL,$0F15
        	LD DE,561
RETURNTOMAP2:
	xor a
	jr RETURNTOMAP
RETURNTOMAP3:
	ld a,5
RETURNTOMAP:
	ld (DUNGEONCHECK),a
	push de	
	LD (X),HL
	ld (COUNTER),a
	LD HL,BITMAPS
	LD (BMPLOC),HL
	LD HL,MAP
	LD (MAPSTART),HL
	pop de
	ADD HL,DE
	jp UPDATEMOVE
TOWER:
	ld a,(SCENARIO)
	cp 9
	jr nc,INTOTHETOWER
	ld de,$1C04
	ld hl,force
	call DISPSTRING	
	jp RETFROMSTATS
INTOTHETOWER:
	ld hl,MAP4+597
	ld (hl),7
	call DRAWWALLS
	ld de,$1C14
	ld hl,last
	call DISPSTRING3
	call ERASESCREEN
	LD DE,369
	ld hl,$0A09
MAP4PLACER:
        	ld a,-1
MAP4PLACER2:
	ld (DUNGEONCHECK),a
	ld bc,MAP4
	ld (MAPVAR),bc
	push de
	LD (X),HL
	LD HL,BITMAPS2
	LD (BMPLOC),HL
	LD HL,MAP4
	LD (MAPSTART),HL
	pop de
	ADD HL,DE
	jp UPDATEMOVE

TOWN:
	ld a,(DUNGEONCHECK)
	cp 5
	jr z,TOTOWN2
	or a
	jr nz,TREASURECHEST
FROMLOAD:
	ld a,9
	ld (DUNGEONCHECK),a
	ld a,(SCENARIO)
	cp 2
	jr nz,MANDEAD
	jr MANALIVE
TOTOWN2:
	ld a,8
	ld (DUNGEONCHECK),a
MANALIVE:
	ld hl,MAP2+814
	ld (hl),5
	jr MANALIVESTILL
MANDEAD:
	ld hl,MAP2+814
	ld (hl),0
MANALIVESTILL:
	xor a
	ld (LEADERCHECK),a
        	LD DE,700
	ld hl,$1310
	jr MAP2PLACER
MAP2PLACER2:
	ld (DUNGEONCHECK),a
MAP2PLACER:
	ld bc,MAP2
	ld (MAPVAR),bc
	push de
	LD (X),HL
	LD HL,BITMAPS2
	LD (BMPLOC),HL
	LD HL,MAP2
	LD (MAPSTART),HL
	pop de
	ADD HL,DE
	jp UPDATEMOVE


TREASURECHEST:
	call DRAWWALLS
	ld hl,YouFind
	ld de,$1820
	call DISPSTRING
	ld a,(DUNGEONCHECK)
	cp -2
	jr nz,RANDTREASURE
FAITHARMOR:
	ld hl,faitharmor
	ld de,$1E20
	call DISPSTRING3
	ld a,10
	ld (SCENARIO),a
	jp LEADER6
RANDTREASURE:
  ld a,r
  srl a
  and 3   
	cp 1
	jr z,FGOLD
	or a
	jr z,FITEM
FSCROLL
	ld hl,scroll
	ld de,$1E20
	call DISPSTRING
	call ADD30LOOP
	ld de,(XP)
	add hl,de
	ld (XP),hl
	jr ITSFOUND
FITEM:
	ld hl,WhichItems
	push hl
	ld a,(hl)
	inc a
	ld (hl),a
	ld hl,MedKit
	ld de,$1E20
	call DISPSTRING
  ld a,r
  srl a
  and 3   
  or a
  jr z,ITSFOUNDPOP
	pop hl
	inc hl
	ld a,(hl)
	inc a
	ld (hl),a
	ld hl,MagKit
	ld de,$2420
	call DISPSTRING
	jr ITSFOUND
FGOLD:
	call ADD30LOOP
	ld de,(GOLD)
	add hl,de
	ld (GOLD),hl
	ld hl,somegold
	ld de,$1E20
	call DISPSTRING
	jr ITSFOUND
ITSFOUNDPOP:
	pop hl
ITSFOUND:

	ld bc,(X)
	push bc
	ld hl,0
LOOPOF362:
	ld de,36
	add hl,de	
	djnz LOOPOF362
	pop bc
	ld b,0
	add hl,bc		
	ld de,73
	add hl,de
	ex de,hl	
	push de
	ld hl,(MAPVAR)
	pop de
	add hl,de
	ld (hl),0
RETFROMSTATS:
	call WaitKey
RETFROMSTATS2:
	call ERASESCREEN
	ld hl,(WHEREAT)
	jp  UPDATEMOVE

BLANK1429:
        	ld bc,$1429
        	ld hl,KuraiBlank
	jp NASRWARP
KURAIMAGIC1:
        	ld bc,$111D
        	ld hl,KuraiMagic1
	jp NASRWARP

STATUSSCREEN:
 ld hl,Routine2
 ld de,TEMPSPACE
 ld bc,768
 ldir
 jp TEMPSPACE

DRAWWIDE:
	ld a,$40
	ld (BKX),a
	ld hl,WIDECURSOR
	call drawit
	ld a,(BKX)
	add a,8		
	ld (BKX),a
	ld hl,WIDECURSOR
	call drawit
	ld a,(BKX)
	add a,8		
	ld (BKX),a
	ld hl,WIDECURSOR
	jp drawit	
DISPLAYSTATS:
	ld hl,HPstr
	ld de,$1217
	call DISPSTRING
		;ld hl,MPstr
	ld de,$1817
	call DISPSTRING
		;ld hl,Strength
	ld de,$1E17
	call DISPSTRING
		;ld hl,Intelligence
	ld de,$2413
	call DISPSTRING
		;ld hl,Speed
	ld de,$2A17
	jp DISPSTRING
DRAWSTATS:
	ld hl,(MAXHP)	
	ld de,$1222
	call DISPHL
	ld hl,(MAXMP)	
	ld de,$1822
	call DISPHL
DRAWSTATS2:
	ld hl,(STRENGTH)	
	ld de,$1E22
	call DISPHL
	ld hl,(INTELLIGENCE)	
	ld de,$2422
	call DISPHL
	ld hl,(SPEED)	
	ld de,$2A22
	jp DISPHL

CASTLEORMAN:
 ld hl,Routine3
 ld de,TEMPSPACE
 ld bc,768
 ldir
 jp TEMPSPACE

CAVEORELSE:
	ld a,(DUNGEONCHECK)
	cp 5
	jp z,OTHERSIDE
	or a
	jp nz,KEYLOOP
SIDE1:
	ld de,13
	ld hl,$000D
	ld a,3
	jp MAP2PLACER2
OTHERSIDE:
	ld bc,(X)
	ld a,b
	cp 8
	jp z,OTHERSIDECAVE
	cp 2
	jp z,OTHERCAVE
OLDMANCAVE:
	LD DE,699
	ld hl,$130F
        	ld a,10
MAP3PLACER:
	ld (DUNGEONCHECK),a
	ld bc,MAP3
	ld (MAPVAR),bc
	push de
	LD (X),HL
	LD HL,BITMAPS2
	LD (BMPLOC),HL
	LD HL,MAP3
	LD (MAPSTART),HL
	pop de
	ADD HL,DE
	jp UPDATEMOVE
OTHERCAVE:
	ld a,(SCENARIO)
	cp 6
	jp c,LOCKED
	LD DE,504
	ld hl,$0E00
        	ld a,4
	jp MAP3PLACER

OTHERSIDECAVE:
	ld de,305
	ld hl,$080B
	ld a,3
	jp MAP2PLACER23
CASTLE:
	ld a,(DUNGEONCHECK)
	cp 5
	jp z,BANDITCASTLE
	LD DE,438
        	LD HL,$0C06
	ld a,1	
MAP2PLACER23: ;;to save bytes
	jp MAP2PLACER2
BANDITCASTLE:
	ld hl,MAP3+653
	ld (hl),7
	call DRAWWALLS
	ld de,$1C10
	ld hl,BanditCastle
	call DISPSTRING3
	call ERASESCREEN
	LD DE,688
        	LD HL,$1304
	ld a,7	
	jp MAP3PLACER
ITEMSHOP:
	ld hl,ItemShop
	call DISPSTRING
		;ld hl,ItemStore
	ld de,$150A
	call DISPSTRING
		;ld hl,ItemMenu
		;ld de,$0E0A
	ld d,$0E
	call DISPSTRING	

FROMISHOP2:
	push hl
FROMISHOP:	
	ld hl,(GOLD)	
	ld de,$3622
	call DISPHL
	pop hl
	ld hl,WhichItems
	push hl	
ISHOPLOOP:
	bcall(_getcsc)
	cp G_1
	jp z,BUYMED
	cp G_2
	jp z,BUYMAG
	cp G_DEL
	jp z,ENDHEAL2
	cp G_3
	jp nz,ISHOPLOOP
BUYBOMB:
	pop hl
	inc hl
	inc hl
	jp ISHOPCONT
ENDHEAL2:
	pop hl
	jp  RETFROMSTATS2
BUYMAG:
	pop hl
	inc hl
	jp ISHOPCONT
BUYMED:
	pop hl
ISHOPCONT:
	push hl
	ld hl,(GOLD)
	ld de,30
	and a
	sbc hl,de
	jp c,FROMISHOP
	ld (GOLD),hl
	pop hl
	ld a,(hl)
	inc a
	ld (hl),a

	ld hl,ThankYou
	ld de,$2D17
	call DISPSTRING
	call DELAY3
	ld hl,BlankText
	ld de,$2D17
	push hl
	call DISPSTRING
	pop hl
		;ld hl,BlankText
		;ld de,$2D24
	ld e,$24
	push hl
	call DISPSTRING
	pop hl
		;ld hl,BlankText
	ld de,$3621
	call DISPSTRING
	jp FROMISHOP2



TALK6:
	ld hl,KuraiText16
	jr KURAITALKSWO32
TALK3:
	ld hl,KuraiText9
	jr KURAITALKS
TALK2:
	ld hl,KuraiText7
	jr KURAITALKSWO3

TALK8:
	ld hl,KuraiText21
	jr KURAITALKSWO32

TALK12:
	ld hl,KuraiText30
	jr KURAITALKSWO3
TALK11:
	ld hl,KuraiText27
	jr KURAITALKS
TALK10:
	ld hl,KuraiText25
	jr KURAITALKSWO32	
TALK9:
	ld hl,KuraiText23
	jr KURAITALKSWO3
TALK7:
	ld hl,KuraiText19
	jr KURAITALKSWO32
TALK4:
	ld hl,KuraiText12
	jr KURAITALKSWO3
TALK5:
	ld hl,KuraiText14
	jr KURAITALKSWO3
TALK1:
	ld a,1
	ld (SCENARIO),a
	ld hl,KuraiText4
	jr KURAITALKS
KURAITALKSWO32:
	call WaitKey ;; pauses, eliminates a WaitKey
KURAITALKSWO3:  ;; Kurai Talks w/o 3rd text
 call KuraiTalks2
 ld hl,BlankText
 jr EndKuraiTalk

KURAITALKS:
 call KuraiTalks2
EndKuraiTalk:
 ld d,$1A
 call DISPSTRING3
 res 3,(IY+05)
 jp ERASESCREEN

KuraiTalks2:
	push hl
	call DRAWWALLS
	call INVERTSCREEN
	ld hl,KuraiFace
	ld bc,$3D36
	call NASRWARP
	pop hl
	set 3,(IY+05)
	ld de,$0C05
	call DISPSTRING
    ld d,$13
	jp DISPSTRING


ATTACKNUM2:
	ld a,(DUMMY1)
	dec a
	ld (BKX),a
	ld b,6
ATTACKLOOP2:
	push bc
	ld a,(BKX)
	inc a
	inc a
	ld (BKX),a
	ld a,7
	ld (BKY),a
	ld hl,ERASUREX
	call drawit
	ld a,12
	ld (BKY),a
	ld hl,ERASUREX
	call drawit
	pop bc
	djnz ATTACKLOOP2
	ret
DUMMY1DISP:
	ld (DUMMY1),hl
	ld de,100
	and a
	sbc hl,de
	jr nc,over100disp
	ld hl,(DUMMY1)
	ld de,$163E
	jp DISPHL	
over100disp:
	ld hl,(DUMMY1)
	jp DISPHL2

REDISP:
	pop hl
	ld a,(DUMMY1)
	or a
	jp z,TEMPSPACE
	call ERASEMENU	
	call DISPMP
	ld hl,WhichSpells
	inc hl
	inc hl
	inc hl
	inc hl
	ld a,(hl)
	or a	
	jr z,NEXT4
	push hl
             ld hl,Sire
	ld de, $1F49                    ;Y=2C,X=1A
	call DISPSTRING
	pop hl
NEXT4:
	inc hl
	ld a,(hl)
	or a
	jr z,NEXT5
	push hl
             ld hl,Rock
	ld de,$2649                    ;Y=2C,X=1A
	call DISPSTRING
	pop hl
NEXT5:
	inc hl
	ld a,(hl)
	or a
	jr z,NEXT6
	push hl
             ld hl,Haste
	ld de,$2D49                    ;Y=2C,X=1A
	call DISPSTRING	
	pop hl
NEXT6:
	inc hl
	ld a,(hl)
	or a
	jr z,DONEWITHM2
	push hl
             ld hl,Doom
	ld de,$3449                    ;Y=2C,X=1A
	call DISPSTRING	
	pop hl
DONEWITHM2:
	ld bc,$1F3F
	ld hl,CURSORMAGIC
	call drawit2
	ld hl,WhichSpells
	inc hl
	inc hl
	inc hl
	inc hl
	jp TEMPSPACE+95

REDISP2:
	pop hl
	ld a,(DUMMY1)
	or a
	jp z,KURAISKILL
	call ERASEMENU	
	call DISPMP
	ld hl,WhichSkills
	inc hl
	inc hl
	inc hl
	inc hl
	ld a,(hl)
	or a	
	jr z,NEXT4S
	push hl
    ld hl,ReLife
	ld de, $1F49                    ;Y=2C,X=1A
	call DISPSTRING
	pop hl
NEXT4S:
	inc hl
	ld a,(hl)
	or a
	jr z,NEXT5S
	push hl
             ld hl,Build
	ld de, $2649                    ;Y=2C,X=1A
	call DISPSTRING
	pop hl
NEXT5S:
	inc hl
	ld a,(hl)
	or a
	jr z,NEXT6S
	push hl
             ld hl,Xfrm
	ld de, $2D49                    ;Y=2C,X=1A
	call DISPSTRING	
	pop hl
NEXT6S:
	inc hl
	ld a,(hl)
	or a
	jr z,DONEWITHS2
	push hl
             ld hl,Bribe
	ld de, $3449                    ;Y=2C,X=1A
	call DISPSTRING	
	pop hl
DONEWITHS2:
	ld bc,$1F3F
	ld hl,CURSORMAGIC
	call drawit2
	ld hl,WhichSkills
	inc hl
	inc hl
	inc hl
	inc hl
	jp TEMPSPACE+95

DELEXIT:
	ld hl,DELtoExit
	ld de,$3434
	jp DISPSTRING
THANKYOU:
	ld hl,ThankYou
	ld de,$2D17
	call DISPSTRING
THANX:
	jp  RETFROMSTATS


REGENLOOPER:
	ld (MP),hl
	ld b,4
REGENLOOP:
	push bc
	ld bc,$2322
	ld hl,RegenEffect
	call NASRWARP
	call DELAY3
	ld bc,$2822
	ld hl,BlankEnemy
	call NASRWARP
	call KURAI
	pop bc
	djnz REGENLOOP	
	ret
HEALLOOPER:
	ld b,55
HEALLOOP:
	push bc
	ld a,r
	srl a
	and 15
	add a,15
	ld (BKX),a
	ld a,r
	srl a
	and 18   
	add a,35
	ld (BKY),a
	ld hl,HEALSPRITE
	call drawit
	ld a,(BKX)
	add a,8
	ld (BKX),a
	ld a,r
	srl a
	and 10   
	add a,40
	ld (BKY),a
	ld hl,HEALSPRITE
	call drawit
	pop bc 
	djnz HEALLOOP 
	ret

CHESTINIT:
	ld (MAPVAR),hl
	ld b,6
	ex de,hl
INITCHESTS:
	push hl
	bcall(_ldhlind)   ;; puts (hl) into hl
	ld de,(MAPVAR)
	add hl,de
	ld (hl),6
	pop hl
	inc hl
	inc hl
	djnz INITCHESTS
	RET
FAITHSWORD:
	ld de,$2210
	ld hl,BanditsText1
DISPSTRING3:
    call DISPSTRING
WaitKey:
	push hl
	bcall(_getcsc)
	pop hl
	cp G_ENTER
	ret z
	cp G_2nd
	jr nz,WaitKey
	RET

FastCopy:
 	push af
	push ix
	push bc
	push hl
	push de

	di
	ld	a,$80				; 7
	out	($10),a				; 11
	ld	hl,GRAPH_MEM-12-(-(12*64)+1)		; 10
	ld	a,$20				; 7
	ld	c,a				; 4
	inc	hl				; 6 waste
	dec	hl				; 6 waste
fastCopyAgain:
	ld	b,64				; 7
	inc	c				; 4
	ld	de,-(12*64)+1			; 10
	out	($10),a				; 11
	add	hl,de				; 11
	ld	de,10				; 10
fastCopyLoop:
	add	hl,de				; 11
	inc	hl				; 6 waste
	inc	hl				; 6 waste
	inc	de				; 6
	ld	a,(hl)				; 7
	out	($11),a				; 11
	dec	de				; 6
	djnz	fastCopyLoop			; 13/8
	ld	a,c				; 4
	cp	$2B+1				; 7
	jr	nz,fastCopyAgain		; 10/1

    pop de
	pop hl
	pop bc
	pop ix
 	pop af
	ret					; 10

RAND:
      ld b,255
RANDN: 
	push	hl
	push	de
	ld	hl,$0404
data = $-2
	ld	a,r
	ld	d,a
	ld	e,(hl)
	add	hl,de
	add	a,l
	xor	h
	ld	(data),hl
	ld	hl,0
	ld	e,a
	ld	d,h
randl:
	add	hl,de
	djnz	randl
	ld	a,h
	pop	de
	pop	hl
	ret

DEADMAN:
 ld hl,Routine3_2
 ld de,TEMPSPACE
 ld bc,768
 ldir
 jp TEMPSPACE

BEGIN2:
 ld hl,Routine1_2
 ld de,TEMPSPACE
 ld bc,768
 ldir
 jp TEMPSPACE

YESNO:
	ld hl,yesorno
	ld de,$0505
	jp DISPSTRING
ADD20LOOP:
	ld de,20
	jp READDLOOP
ADD30LOOP:
	ld de,30
READDLOOP:
	ld a,(KURAILEVEL)
	ld hl,0
ADDINLOOP:
	add hl,de
	dec a
	jp nz,ADDINLOOP
	ret

MAGIDAMAGE:
	call RANDOM7
	call DUMMY1DISP
             ld hl,damage
	ld de, $1647     
	call DISPSTRING
	call ERASETEXT2
	ld a,(ENEMYSPEED1)
	or a
	jp z,NEXTDAMAGE1
	ld a,1
	ld (WHICHENEMY),a
	ld hl,(ENEMYHP1)
	ld de,(DUMMY1)
	and a
	sbc hl, de
	ld (ENEMYHP1),hl
	call c,ENEMYDEAD
	cp 199
	jp z,FROMBATTLE	
NEXTDAMAGE1:
	ld a,(ENEMYSPEED2)
	or a
	jp z,NEXTDAMAGE2
	ld a,2
	ld (WHICHENEMY),a
	ld hl,(ENEMYHP2)
	ld de,(DUMMY1)
	and a
	sbc hl, de
	ld (ENEMYHP2),hl
	call c,ENEMYDEAD
	cp 199
	jp z,FROMBATTLE	
NEXTDAMAGE2:
	ld a,(ENEMYSPEED3)
	or a
	jp z,MAGIDONE
	ld a,3
	ld (WHICHENEMY),a
	ld hl,(ENEMYHP3)
	ld de,(DUMMY1)
	and a
	sbc hl, de
	ld (ENEMYHP3),hl
	call c,ENEMYDEAD
	cp 199
	jp z,FROMBATTLE	
	jp MAGIDONE

REGENEFFECT:
	ld a,(KURAILEVEL)
	ld b,a
	ld hl,0
LEVEL_LOOP3:
	ld de,10
	add hl,de
	djnz LEVEL_LOOP3
	call OVERMAXHP
	ld hl,(HP)		;; Displays Kurai's HP
	ld de,$1E4C
	jp DISPHL

ENEMYDEAD:
	ld hl,0
	ld a,(WHICHENEMY)
	cp 2	
	jp z,KILL2
	cp 3
	jp z,KILL3
	ld (ENEMYSPEED1),hl
	ld bc,$0F39
	jp CONTKILL
KILL2:
	ld (ENEMYSPEED2),hl
	ld bc,$2539
	jp CONTKILL
KILL3:
	ld (ENEMYSPEED3),hl
	ld bc,$3B39
CONTKILL:	
	push bc
	ld hl,EnemyDead
	call NASRWARP
	call DELAY2
	pop bc
	ld hl,BlankEnemy
	call NASRWARP	
	ld hl,(ENEMYST1)
	srl h
	rr l
	ld de,(XPGAINED)
	add hl,de
	ld (XPGAINED),hl
	ld a,(NUMBEROFENEMIES)
	dec a
	ld (NUMBEROFENEMIES),a
	or a
	ret NZ
	jp YouWin

AuthorTxt:
 .db "Par: Kurai Highsmith & SBH",0
 .db "Traduit par: DJ_Omnimaga",0
 

#include "dlib.inc"
#include "deadmap.inc"
#ifdef en
	#include "deadtext_en.inc"
#else
	#include "deadtext_fr.inc"
#endif
.end
